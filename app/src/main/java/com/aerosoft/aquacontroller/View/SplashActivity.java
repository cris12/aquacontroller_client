package com.aerosoft.aquacontroller.View;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.Toast;

import com.aerosoft.aquacontroller.Controller.DataHelper.ConnectionHelper;
import com.aerosoft.aquacontroller.Controller.DataHelper.UdpHelper;
import com.aerosoft.aquacontroller.Controller.DataListener.IDataIPEventListener;
import com.aerosoft.aquacontroller.Controller.DataListener.IRequestListener;
import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.aerosoft.aquacontroller.Controller.DataProvider.RequestManager;
import com.aerosoft.aquacontroller.Controller.DataProvider.UDPBackGroundService;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.View.Fragments.Preference.ShareManager;
import com.aerosoft.aquacontroller.View.Fragments.Preference.DialogPreferences;
import com.aerosoft.aquacontroller.View.ViewAdapter.ListIPAdapter;
import com.aerosoft.aquacontroller.View.ViewHelper.ToastHelper;
import com.eftimoff.androipathview.PathView;

import java.util.EventObject;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_CANAL_STATE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_DAILY_TIMER_SATE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_HOURS_TIMER_SATE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_INFO;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_SECOND_TIMER_SATE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_TEMP_SENSOR;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_TEMP_STATE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_CONNECTION;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DEVICE_CHANAL_STATE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DEVICE_INFO;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DEVICE_TEMP;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DEVICE_TEMP_SENSOR;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DEVICE_TIME_DAILY;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DEVICE_TIME_HOUR;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DEVICE_TIME_SECOND;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_INIT_LOGO;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_SETTINGS;


public class SplashActivity extends AppCompatActivity implements IDataIPEventListener {


    private SplashActivity currentActivity;
    private int attemptCount = com.aerosoft.aquacontroller.Controller.DataHelper.Constants.ATTEMPT_COUNT;
    private boolean isConnected = false;
    private Snackbar snackbar;
    private FloatingActionButton fab;
    private ImageButton settingsButton;
    private PathView[] pathViewImage;
    private int indexTaskQuery = 0;
    private int indexTaskPathView = 0;
    private RecyclerView listIp;
    private ProtocolHelper.REQUEST_TYPE[] taskProtocol = {
            REQUEST_DEVICE_INFO,
            REQUEST_DEVICE_CANAL_STATE,
            REQUEST_DEVICE_DAILY_TIMER_SATE,
            REQUEST_DEVICE_HOURS_TIMER_SATE,
            REQUEST_DEVICE_SECOND_TIMER_SATE,
            REQUEST_DEVICE_TEMP_STATE,
            REQUEST_DEVICE_TEMP_SENSOR};

    private TaskResult[] taskQuery = {
            RESULT_DEVICE_INFO,
            RESULT_DEVICE_CHANAL_STATE,
            RESULT_DEVICE_TIME_DAILY,
            RESULT_DEVICE_TIME_HOUR,
            RESULT_DEVICE_TIME_SECOND,
            RESULT_DEVICE_TEMP,
            RESULT_DEVICE_TEMP_SENSOR};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        currentActivity = this;
        super.onCreate(savedInstanceState);
        ShareManager.getInstance(this).Initilized();
        Intent intentService = new Intent(this, UDPBackGroundService.class);
        startService(intentService);
        setContentView(R.layout.activity_main);
        ToastHelper.Initilized(getApplicationContext());
        pathViewImage = new PathView[]{
                findViewById(R.id.deviceConnectPathView),
                findViewById(R.id.deviceInfoPathView),
                findViewById(R.id.deviceChanalPathView),
                findViewById(R.id.deviceTimerPathView),
                findViewById(R.id.deviceTimerHoursPathView),
                findViewById(R.id.deviceTimerSecondPathView),
                findViewById(R.id.deviceTempPathView),
                findViewById(R.id.deviceRealTempPathView),
                findViewById(R.id.deviceSettingsPathView)};
        fab = findViewById(R.id.fabSplash);
        listIp = findViewById(R.id.ip_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        listIp.setLayoutManager(linearLayoutManager);
        if (ShareManager.getInstance(this).GetAllIP().size() > 0) {
            listIp.setAdapter(new ListIPAdapter(this, this));
        } else {
            listIp.setVisibility(View.INVISIBLE);
        }
        settingsButton = findViewById(R.id.imageButton);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent settingsActivity = new Intent(getBaseContext(),
                        DialogPreferences.class);
                startActivity(settingsActivity);
            }
        });
        fab.setVisibility(View.INVISIBLE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, getString(R.string.dialog_try_again), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                fab.setVisibility(INVISIBLE);
                settingsButton.setVisibility(INVISIBLE);
                CheckAccessDevice();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ShareManager.getInstance(this).GetAllIP().size() > 0) {
            listIp.setVisibility(View.VISIBLE);
            listIp.setAdapter(new ListIPAdapter(this, this));
        } else {
            listIp.setVisibility(View.INVISIBLE);
            Intent settingsActivity = new Intent(getBaseContext(),
                    DialogPreferences.class);
            startActivity(settingsActivity);
        }
    }

    private void CheckAccessDevice() {
        if (!UdpHelper.ADDRESS.isEmpty()) {
            new ConnectionHelper().getConnectedDevices(new IRequestListener() {
                @Override
                public void onEvent(EventObject e) {
                    if (e.getSource() == TaskResult.RESULT_SUCCESS) {
                        isConnected = true;
                        StartAnimation(RESULT_CONNECTION);
                    } else {
                        attemptCount--;
                        if (attemptCount < 0) {
                            fab.setVisibility(VISIBLE);
                            settingsButton.setVisibility(VISIBLE);
                            listIp.setVisibility(VISIBLE);
                            Toast.makeText(getApplicationContext(), ProtocolHelper.GetMessageToast(currentActivity, (TaskResult) (e.getSource())),
                                    Toast.LENGTH_LONG).show();

                        } else {
                            CheckAccessDevice();
                        }
                    }

                }
            });
        } else {
            Intent settingsActivity = new Intent(getBaseContext(),
                    DialogPreferences.class);
            startActivity(settingsActivity);

        }
    }


    private void GetSoftwareSettings() {
        StartAnimation(RESULT_SETTINGS);
    }

    private void StartActivity() {
        Intent intent = new Intent(currentActivity, DeviceActivity.class);
        startActivity(intent);
        currentActivity.finish();
    }


    private void GetDataController() {

        new RequestManager().SendGETRequest(taskProtocol[indexTaskQuery], new IRequestListener() {
            @Override
            public void onEvent(EventObject e) {
                if (e != null && e.getSource() == TaskResult.RESULT_SUCCESS) {

                    if (taskQuery.length > indexTaskQuery) {
                        StartAnimation(taskQuery[indexTaskQuery]);
                        indexTaskQuery++;
                    }
                } else {
                    attemptCount--;
                    if (attemptCount < 0) {
                        fab.setVisibility(VISIBLE);
                        settingsButton.setVisibility(VISIBLE);
                        listIp.setVisibility(VISIBLE);
                        if (e != null) {
                           ToastHelper.SnackBarShow(findViewById(R.id.fab),e.getSource().toString());
                        } else {
                            ToastHelper.SnackBarShow(findViewById(R.id.fab),TaskResult.RESULT_DATA_CORRUPTED);
                        }
                    } else {
                        GetDataController();
                    }
                }
            }
        });
    }

    private void StartAnimation(final TaskResult task) {
        final int delay = 200;
        int duration = 200;
        attemptCount = com.aerosoft.aquacontroller.Controller.DataHelper.Constants.ATTEMPT_COUNT;
        pathViewImage[indexTaskPathView].getPathAnimator().delay(delay).duration(duration).listenerEnd(new PathView.AnimatorBuilder.ListenerEnd() {
            @Override
            public void onAnimationEnd() {
                    indexTaskPathView++;
                switch (task) {
                    case RESULT_INIT_LOGO:
                        CheckAccessDevice();
                        break;
                    case RESULT_SETTINGS:
                        if (isConnected){
                        StartActivity();}
                        break;
                    case RESULT_DEVICE_TEMP_SENSOR:
                        if (isConnected){
                        GetSoftwareSettings();}
                        break;
                    default:
                        if (isConnected){
                        GetDataController();}
                        break;
                }

            }
        }).interpolator(new AccelerateDecelerateInterpolator()).start();


    }

    @Override
    public void Event() {
        listIp.setAdapter(new ListIPAdapter(this, this));
    }

    @Override
    public void Event(String ip) {
        UdpHelper.ADDRESS = ip;
        CheckAccessDevice();
        listIp.setVisibility(INVISIBLE);
        fab.setVisibility(INVISIBLE);
        settingsButton.setVisibility(INVISIBLE);
    }
}
