package com.aerosoft.aquacontroller.Model.DataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Vadim on 22.11.2016.
 */

public class DeviceHoursTimer implements Cloneable, IValidateData {
    @SerializedName("ht_m_st")
    private ArrayList<Integer> hours_timer_min_start;
    @SerializedName("ht_m_sp")
    private ArrayList<Integer> hours_timer_min_stop;
    @SerializedName("ht_s")
    private ArrayList<Integer> hours_timer_state;
    @SerializedName("ht_c")
    private ArrayList<Integer> hours_timer_canal;

    public ArrayList<Integer> getHours_timer_min_start() {
        return hours_timer_min_start;
    }

    public ArrayList<Integer> getHours_timer_min_stop() {
        return hours_timer_min_stop;
    }

    public ArrayList<Integer> getHours_timer_state() {
        return hours_timer_state;
    }

    public ArrayList<Integer> getHours_timer_canal() {
        return hours_timer_canal;
    }


    public DeviceHoursTimer clone() throws CloneNotSupportedException {
        DeviceHoursTimer newDeviceHoursTimer = (DeviceHoursTimer) super.clone();
        newDeviceHoursTimer.hours_timer_min_start = (ArrayList<Integer>) hours_timer_min_start.clone();
        newDeviceHoursTimer.hours_timer_min_stop = (ArrayList<Integer>) hours_timer_min_stop.clone();
        newDeviceHoursTimer.hours_timer_state = (ArrayList<Integer>) hours_timer_state.clone();
        newDeviceHoursTimer.hours_timer_canal = (ArrayList<Integer>) hours_timer_canal.clone();
        return newDeviceHoursTimer;
    }

    @Override
    public boolean IsValidate() {
        if (hours_timer_min_start == null || hours_timer_min_stop == null || hours_timer_state == null || hours_timer_canal == null)
            return false;
        int size = hours_timer_min_start.size();
        return !(size != hours_timer_min_stop.size() || size != hours_timer_state.size() || size != hours_timer_canal.size());
    }
}
