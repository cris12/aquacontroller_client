package com.aerosoft.aquacontroller.View.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.Controller.DataProvider.RequestManager;
import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceCanalState;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.View.DeviceActivity;
import com.aerosoft.aquacontroller.View.ViewHelper.ToastHelper;
import com.aerosoft.aquacontroller.Controller.DataListener.IDataCanalEventListener;
import com.aerosoft.aquacontroller.Controller.DataListener.IRequestListener;
import com.aerosoft.aquacontroller.View.ViewAdapter.ListCanalsAdapter;

import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.*;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.EventObject;

import static android.view.View.INVISIBLE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DATA_DEVICE_BUSY;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CanalFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CanalFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CanalFragment extends Fragment implements IRequestListener {


    private OnFragmentInteractionListener mListener;
    private ListView canalList;
    private View listHeader;
    private FloatingActionButton fab;
    private DeviceCanalState changedDeviceCanalState;
    private DeviceInfo deviceInfo;
    private DeviceCanalState event;
    int attemptCount = com.aerosoft.aquacontroller.Controller.DataHelper.Constants.ATTEMPT_COUNT;
    private Snackbar snackbar;

    public CanalFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ChanalFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CanalFragment newInstance() {
        CanalFragment fragment = new CanalFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chanal, container, false);
        listHeader = inflater.inflate(R.layout.chanal_list_header, null);
        canalList = view.findViewById(R.id.chanalDeviceListView);
        fab = getActivity().findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!((DeviceActivity) getActivity()).isWait) {
                    ((DeviceActivity) getActivity()).isWait = true;
                    snackbar = Snackbar.make(v, getString(R.string.SendDataMessage), Snackbar.LENGTH_INDEFINITE)
                            .setAction("Action", null);
                    snackbar.show();
                    fab.setVisibility(INVISIBLE);
                    if (changedDeviceCanalState != null) {
                        attemptCount = com.aerosoft.aquacontroller.Controller.DataHelper.Constants.ATTEMPT_COUNT;
                        new RequestManager().SendPOSTRequest(ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_CANAL_STATE, changedDeviceCanalState, CanalFragment.this);
                    }
                } else {
                    ToastHelper.ToastDeviceLog(getActivity(), RESULT_DATA_DEVICE_BUSY);
                }
            }
        });
        return view;
    }

    @Override
    public void onEvent(EventObject e) {


        if (e == null || e.getSource() != TaskResult.RESULT_SUCCESS || !event.getCanal_timer().equals(changedDeviceCanalState.getCanal_timer())) {
            Log.e(getClass().getName(), "Invalid result Canal State");
            attemptCount--;
            if (attemptCount > 0)
                new RequestManager().SendPOSTRequest(ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_CANAL_STATE, changedDeviceCanalState, CanalFragment.this);
            else {
                fab.setVisibility(View.VISIBLE);
                ToastHelper.ToastDeviceLog(e);
                if (getActivity() != null)
                    ((DeviceActivity) getActivity()).isWait = false;
            }
        } else {

            try {
                CreateAdapter();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            if (getActivity() != null)
                ((DeviceActivity) getActivity()).isWait = false;
            ToastHelper.ToastDeviceLog(e);
            changedDeviceCanalState = null;

        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        try {
            CreateAdapter();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        changedDeviceCanalState = null;
        fab.setVisibility(INVISIBLE);
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onStart() {
        super.onStart();
        fab.setVisibility(INVISIBLE);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceInfo deviceInfo) {
        if (deviceInfo != null) {
            this.deviceInfo = deviceInfo;
        }
        if (changedDeviceCanalState == null) {
            try {
                CreateAdapter();
            } catch (CloneNotSupportedException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceCanalState event) {
        if (event != null) {
            this.event = event;
        }
        if (changedDeviceCanalState == null) {
            try {
                CreateAdapter();
            } catch (CloneNotSupportedException e1) {
                e1.printStackTrace();
            }
        }

    }

    private void CreateAdapter() throws CloneNotSupportedException {
        if (this.event == null || this.deviceInfo == null) return;
        if (snackbar != null)
            snackbar.dismiss();
        ListCanalsAdapter adapter = new ListCanalsAdapter(getActivity(), event.clone(), deviceInfo, new IDataCanalEventListener() {
            @Override
            public void onEvent(DeviceCanalState e) {
                if (getActivity() != null && !((DeviceActivity) getActivity()).isWait)
                    fab.setVisibility(View.VISIBLE);
                //cloneable object
                changedDeviceCanalState = e;
            }
        });
        // canalList.addHeaderView(listHeader);
        canalList.setAdapter(adapter);
    }
}
