package com.aerosoft.aquacontroller.View.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.aerosoft.aquacontroller.Controller.DataProvider.RequestManager;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTempSensors;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTempState;
import com.aerosoft.aquacontroller.View.DeviceActivity;
import com.aerosoft.aquacontroller.View.ViewHelper.ToastHelper;
import com.aerosoft.aquacontroller.Controller.DataListener.IDataTempStateListener;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.Controller.DataListener.IRequestListener;
import com.aerosoft.aquacontroller.View.ViewAdapter.TempStateAdapter;

import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.*;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.EventObject;

import static android.view.View.INVISIBLE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DATA_DEVICE_BUSY;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TemperaturaFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TemperaturaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TemperaturaFragment extends Fragment implements IRequestListener {


    private OnFragmentInteractionListener mListener;
    private FloatingActionButton fab;
    private RecyclerView mRecyclerView;
    private DeviceTempState event;
    private DeviceInfo deviceInfo;
    private DeviceTempState changedDeviceTempState;
    private LinearLayout lSensorNotFound;
    private DeviceTempSensors deviceTempSensors;
    int attemptCount = com.aerosoft.aquacontroller.Controller.DataHelper.Constants.ATTEMPT_COUNT;
    private int _countSensor;
    private Snackbar snackbar;

    public TemperaturaFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TemperaturaFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TemperaturaFragment newInstance() {
        TemperaturaFragment fragment = new TemperaturaFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_temperatura, container, false);
        //  listHeader = inflater.inflate(R.layout.chanal_list_header, null);
        mRecyclerView = view.findViewById(R.id.recycler_view_temp_state);
        lSensorNotFound = view.findViewById(R.id.layout_sensornotfound);
        lSensorNotFound.setVisibility(View.GONE);
        //  mRecyclerView.addItemDecoration(new DividerItemDecoration(mRecyclerView.getContext(), null));
        fab = getActivity().findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!((DeviceActivity) getActivity()).isWait) {
                    ((DeviceActivity) getActivity()).isWait = true;
                    snackbar = Snackbar.make(v, getString(R.string.SendDataMessage), Snackbar.LENGTH_INDEFINITE)
                            .setAction("Action", null);
                    snackbar.show();
                    fab.setVisibility(INVISIBLE);
                    if (changedDeviceTempState != null) {
                        new RequestManager().SendPOSTRequest(REQUEST_TYPE.REQUEST_DEVICE_TEMP_STATE, changedDeviceTempState, TemperaturaFragment.this);
                    }
                } else {
                    ToastHelper.ToastDeviceLog(getActivity(), RESULT_DATA_DEVICE_BUSY);
                }

            }
        });

        return view;

    }

    @Override
    public void onEvent(EventObject e) {

        if (e == null || e.getSource() != TaskResult.RESULT_SUCCESS || !onCheckValidResult()) {
            attemptCount--;
            if (attemptCount > 0)
                new RequestManager().SendPOSTRequest(REQUEST_TYPE.REQUEST_DEVICE_TEMP_STATE, changedDeviceTempState, TemperaturaFragment.this);
            else {
                if (getActivity() != null)
                ((DeviceActivity) getActivity()).isWait = false;
                fab.setVisibility(View.VISIBLE);
                ToastHelper.ToastDeviceLog(e);
            }
        } else {
            try {
                CreateListAdapter();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            if (getActivity() != null)
            ((DeviceActivity) getActivity()).isWait = false;
            ToastHelper.ToastDeviceLog(e);
            changedDeviceTempState = null;
        }
    }

    private boolean onCheckValidResult() {
        if (!this.event.getTemp_timer_chanal().equals(changedDeviceTempState.getTemp_timer_chanal()))
            return false;
        if (!this.event.getTemp_timer_max_end().equals(changedDeviceTempState.getTemp_timer_max_end()))
            return false;
        if (!this.event.getTemp_timer_min_start().equals(changedDeviceTempState.getTemp_timer_min_start()))
            return false;
        return this.event.getTemp_timer_state().equals(changedDeviceTempState.getTemp_timer_state());
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        changedDeviceTempState = null;
        fab.setVisibility(INVISIBLE);
    }

    @Override
    public void onStart() {
        super.onStart();
        fab.setVisibility(INVISIBLE);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceInfo deviceInfo) {
        if (deviceInfo != null) {
            this.deviceInfo = deviceInfo;
            if (changedDeviceTempState == null) {
                try {
                    CreateListAdapter();
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceTempSensors deviceTempSensors) {
        if (deviceInfo != null) {
            this.deviceTempSensors = deviceTempSensors;
            if (_countSensor != deviceTempSensors.getAvailableSensorsCount()) {
                _countSensor = deviceTempSensors.getAvailableSensorsCount();
                if (_countSensor > 0) {
                    lSensorNotFound.setVisibility(View.GONE);
                } else {
                    lSensorNotFound.setVisibility(View.VISIBLE);
                }
                if (changedDeviceTempState == null) {
                    try {
                        CreateListAdapter();
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceTempState event) {
        if (event != null) {
            this.event = event;
            if (changedDeviceTempState == null) {
                try {
                    CreateListAdapter();
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void CreateListAdapter() throws CloneNotSupportedException {
        if (this.event == null || this.deviceInfo == null || deviceTempSensors == null) return;
        if (snackbar != null)
            snackbar.dismiss();
        for (Integer sensor : deviceTempSensors.getSensors()) {
            if (sensor > 0) {
                mRecyclerView.setAdapter(new TempStateAdapter(getContext(), event.clone(), this.deviceInfo, this.deviceTempSensors, new IDataTempStateListener() {
                    @Override
                    public void onEvent(DeviceTempState e) {
                        if (getActivity() != null && !((DeviceActivity) getActivity()).isWait)
                        fab.setVisibility(View.VISIBLE);
                        //cloneable object
                        changedDeviceTempState = e;
                    }
                }));
                mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                return;
            }
        }


    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
