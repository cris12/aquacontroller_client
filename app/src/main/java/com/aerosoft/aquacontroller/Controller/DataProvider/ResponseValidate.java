package com.aerosoft.aquacontroller.Controller.DataProvider;

import android.util.Log;

import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.aerosoft.aquacontroller.Controller.DataHelper.UdpHelper;
import com.aerosoft.aquacontroller.Model.DataModel.IValidateData;
import com.aerosoft.aquacontroller.Model.TaskResponse.TaskResponse;
import com.crashlytics.android.Crashlytics;

/**
 * Created by Doc on 02.10.2017.
 */

public class ResponseValidate {

    public <T extends Cloneable> boolean ValidateResult(TaskResponse<T> taskResponse, ProtocolHelper.REQUEST_TYPE request_type) {
        boolean result = false;
        switch (request_type) {
            case UNDEFINED:
                return false;
            case REQUEST_DEVICE_INFO:
            case REQUEST_DEVICE_TEMP_SENSOR:
            case REQUEST_DEVICE_CANAL_STATE:
            case REQUEST_DEVICE_TEMP_STATE:
            case REQUEST_DEVICE_DAILY_TIMER_SATE:
            case REQUEST_DEVICE_HOURS_TIMER_SATE:
            case REQUEST_DEVICE_SECOND_TIMER_SATE:
            case REQUEST_DEVICE_LOG_INFO:
                result = ((IValidateData) taskResponse.getResponse()).IsValidate();
                if(!result){
                    Crashlytics.log("Data validate error: " + request_type+ " " + request_type);
                    Log.e("ValidateResult.class", "Data validate error: " + request_type+ " " + request_type);
                }
                break;
            case REQUEST_DEVICE_NTP_UPDATE:
                if(taskResponse.getStatus().contains(UdpHelper.RESPONSE_SUCCESS))
                    result = true;
                break;
        }
        return result;
    }

}
