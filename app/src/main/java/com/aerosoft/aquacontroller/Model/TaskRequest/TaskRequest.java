package com.aerosoft.aquacontroller.Model.TaskRequest;

import android.support.annotation.Nullable;
import android.util.Log;

import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.aerosoft.aquacontroller.Controller.DataProvider.ResponseValidate;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceCanalState;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceEpochTime;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceHoursTimer;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceSecondsTimer;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTempSensors;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTempState;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTimer;
import com.aerosoft.aquacontroller.Model.TaskResponse.TaskResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.PROTOCOL.UNDEFINED;

/**
 * Created by Vadim on 27.01.2017.
 */

public abstract class TaskRequest implements ITaskRequest {

    protected ProtocolHelper.REQUEST_TYPE request_type;
    protected String request = "";
    protected Object jsonObject;

    private ProtocolHelper.PROTOCOL protocol = UNDEFINED;


    Class<? extends Object> type = null;

    protected TaskRequest(ProtocolHelper.REQUEST_TYPE request_type, Object jsonObject, ProtocolHelper.PROTOCOL protocol) {
        this.request_type = request_type;
        this.jsonObject = jsonObject;
        this.protocol = protocol;
        InitializedRequest();
    }


    public String GetJSONRequest() {
        return request;
    }

    public TaskResponse<Cloneable> TaskRequestComplete(String json) {
        TaskResponse<Cloneable> result = null;
        if (json != null) {
            try {
                result = GetTaskResponse(json);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @Nullable
    private <T extends Cloneable> TaskResponse<T> GetTaskResponse(String json) throws Exception {
        Type collectionType = null;
        switch (request_type) {
            case UNDEFINED:
                this.type = null;
                break;
            case REQUEST_DEVICE_INFO:
                this.type = DeviceInfo.class;
                collectionType = new TypeToken<TaskResponse<DeviceInfo>>() {
                }.getType();
                break;
            case REQUEST_DEVICE_CANAL_STATE:
                this.type = DeviceCanalState.class;
                collectionType = new TypeToken<TaskResponse<DeviceCanalState>>() {
                }.getType();
                break;
            case REQUEST_DEVICE_TEMP_SENSOR:
                this.type = DeviceTempSensors.class;
                collectionType = new TypeToken<TaskResponse<DeviceTempSensors>>() {
                }.getType();
                break;
            case REQUEST_DEVICE_TEMP_STATE:
                this.type = DeviceTempSensors.class;
                collectionType = new TypeToken<TaskResponse<DeviceTempState>>() {
                }.getType();
                break;
            case REQUEST_DEVICE_DAILY_TIMER_SATE:
                this.type = DeviceTimer.class;
                collectionType = new TypeToken<TaskResponse<DeviceTimer>>() {
                }.getType();
                break;
            case REQUEST_DEVICE_HOURS_TIMER_SATE:
                this.type = DeviceHoursTimer.class;
                collectionType = new TypeToken<TaskResponse<DeviceHoursTimer>>() {
                }.getType();
                break;
            case REQUEST_DEVICE_SECOND_TIMER_SATE:
                this.type = DeviceSecondsTimer.class;
                collectionType = new TypeToken<TaskResponse<DeviceSecondsTimer>>() {
                }.getType();
                break;
            case REQUEST_DEVICE_NTP_UPDATE:
                this.type = DeviceEpochTime.class;
                collectionType = new TypeToken<TaskResponse<DeviceEpochTime>>() {
                }.getType();
                break;

            case REQUEST_DEVICE_LOG_INFO:
                throw new Exception("Not yet used");
            default:
                this.type = null;
                break;
        }
        TaskResponse<T> result = null;
        try {
            result = new Gson().fromJson(json, collectionType);
           if(new ResponseValidate().ValidateResult(result, request_type))
               return result;
            else
               return null;
        } catch (Exception e) {
            Log.e(e.getClass().getName(), e.getMessage());
            return null;
        }
    }


}
