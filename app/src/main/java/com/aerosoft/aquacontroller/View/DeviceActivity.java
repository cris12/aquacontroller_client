package com.aerosoft.aquacontroller.View;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aerosoft.aquacontroller.Controller.DataListener.IRequestListener;
import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.aerosoft.aquacontroller.Controller.DataProvider.RequestManager;
import com.aerosoft.aquacontroller.Controller.DataProvider.UDPBackGroundService;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceEpochTime;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.View.Fragments.CanalFragment;
import com.aerosoft.aquacontroller.View.Fragments.Chart.ChartTempFragment;
import com.aerosoft.aquacontroller.View.Fragments.NotifyFragment;
import com.aerosoft.aquacontroller.View.Fragments.Preference.SettingsFragment;
import com.aerosoft.aquacontroller.View.Fragments.Preference.ShareManager;
import com.aerosoft.aquacontroller.View.Fragments.StateFragment;
import com.aerosoft.aquacontroller.View.Fragments.TemperaturaFragment;
import com.aerosoft.aquacontroller.View.Fragments.TimersFragment;
import com.aerosoft.aquacontroller.View.Fragments.timers.TimersDailyFragment;
import com.aerosoft.aquacontroller.View.Fragments.timers.TimersHoursFragment;
import com.aerosoft.aquacontroller.View.Fragments.timers.TimersSecondFragment;
import com.aerosoft.aquacontroller.View.ViewHelper.ToastHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.EventObject;

import static android.view.View.INVISIBLE;

public class DeviceActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        // SettingsFragment.OnFragmentInteractionListener,
        StateFragment.OnFragmentInteractionListener,
        TemperaturaFragment.OnFragmentInteractionListener,
        CanalFragment.OnFragmentInteractionListener,
        NotifyFragment.OnFragmentInteractionListener,
        TimersFragment.OnFragmentInteractionListener,
        TimersDailyFragment.OnFragmentInteractionListener,
        TimersHoursFragment.OnFragmentInteractionListener,
        TimersSecondFragment.OnFragmentInteractionListener,
        ChartTempFragment.OnFragmentInteractionListener,
        IRequestListener {

    private TextView textVertion;
    private FloatingActionButton fab;
    private DeviceEpochTime time;
    private Snackbar snackbar;
    private int attemptCount;
    public boolean isWait = false;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = findViewById(R.id.fab);
        fab.setVisibility(INVISIBLE);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        textVertion = header.findViewById(R.id.textVertion);
        navigationView.setNavigationItemSelectedListener(this);
        SetFragmentOnActivity(StateFragment.class);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.device, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            SetFragmentOnActivity(SettingsFragment.class);
            return true;
        }
        if (id == R.id.action_time_update) {
            isWait = true;
            time = new DeviceEpochTime();

            snackbar = Snackbar.make(getWindow().getDecorView().findViewById(R.id.fab), getString(R.string.SendDataMessage), Snackbar.LENGTH_INDEFINITE)
                    .setAction("Action", null);
            snackbar.show();
            try {
                time.setEpochTime(System.currentTimeMillis() / 1000 + 3600L * ShareManager.getInstance(this).GetTimeZone());
            } catch (Exception e) {
                time.setEpochTime(System.currentTimeMillis() / 1000);
            }
            attemptCount = com.aerosoft.aquacontroller.Controller.DataHelper.Constants.ATTEMPT_COUNT;
            new RequestManager().SendPOSTRequest(ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_NTP_UPDATE, time, this);
            return true;
        }
        if (id == R.id.action_change_device) {
            Intent intent = new Intent(this, SplashActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        Class fragmentClass = null;
        // Handle navigation view item clicks here.

        switch (item.getItemId()) {
            case R.id.nav_state:
                fragmentClass = StateFragment.class;
                break;
            case R.id.nav_chanal:
                fragmentClass = CanalFragment.class;
                break;
            case R.id.nav_timers:
                fragmentClass = TimersFragment.class;
                break;
            case R.id.nav_temperature:
                fragmentClass = TemperaturaFragment.class;
                break;
            case R.id.nav_settings:
                fragmentClass = SettingsFragment.class;
                break;
            case R.id.nav_notify:
                fragmentClass = NotifyFragment.class;
                break;
            default:
                fragmentClass = StateFragment.class;
                break;
        }

        SetFragmentOnActivity(fragmentClass);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        // Highlight the selected item has been done by NavigationView
        item.setChecked(true);
        // Set action bar title
        setTitle(item.getTitle());
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void SetFragmentOnActivity(Class fragmentClass) {

        Fragment fragment = null;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragmentContent, fragment).commit();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceInfo event) {
       textVertion.setText(event.getVersion());
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onEvent(EventObject e) {
        if (e != null && e.getSource() == ProtocolHelper.TaskResult.RESULT_SUCCESS) {
            isWait = false;
            snackbar.dismiss();
            ToastHelper.ToastDeviceLog(getApplicationContext(), (ProtocolHelper.TaskResult) e.getSource());
        } else {
            attemptCount--;
            if (attemptCount < 0) {
                snackbar.dismiss();
                isWait = false;
                if (e != null) {
                    ToastHelper.ToastDeviceLog(getApplicationContext(), (ProtocolHelper.TaskResult) e.getSource());
                } else {
                    ToastHelper.ToastDeviceLog(getApplicationContext(), ProtocolHelper.TaskResult.RESULT_DATA_CORRUPTED);
                }
            } else {
                new RequestManager().SendPOSTRequest(ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_NTP_UPDATE, time, this);
            }
        }
    }


}
