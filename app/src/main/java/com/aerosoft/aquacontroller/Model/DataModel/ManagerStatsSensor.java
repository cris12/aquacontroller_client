package com.aerosoft.aquacontroller.Model.DataModel;

import android.content.Context;
import android.util.Log;

import com.aerosoft.aquacontroller.Controller.DataHelper.UdpHelper;
import com.aerosoft.aquacontroller.Model.TaskResponse.TaskResponse;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by Doc on 09.10.2017.
 */

public class ManagerStatsSensor {
    private static final String KEY_SENSOR = "sensor";
    private static final String KEY_DATE = "date";
    private static final int MAX_SIZE_ARRAY = 5760;
    private Context context;
    private Map<Integer, List<Integer>> SensorState;
    private Map<Integer, List<Date>> SensorTime;
    private boolean isInit = false;


    private static ManagerStatsSensor instance;
    private int maxTempSensor;

    public Map<Integer, List<Integer>> getSensorState() {
        return SensorState;
    }

    public Map<Integer, List<Date>> getSensorTime() {
        return SensorTime;
    }

    private ManagerStatsSensor(Context context) {
        this.context = context;
        Hawk.init(context).build();
    }

    public static ManagerStatsSensor getInstance(Context context) {
        if (instance == null) {
            instance = new ManagerStatsSensor(context);
        }
        return instance;
    }

    /**
     * @param deviceInfo
     */
    public void Initialized(DeviceInfo deviceInfo) {
        if (!isInit) {
            this.maxTempSensor = deviceInfo.getMax_temp_sensor();
            SensorState = new HashMap<>();
            SensorTime = new HashMap<>();
            for (int i = 0; i < maxTempSensor; i++) {
                List<Integer> listInt = new ArrayList<>();
                listInt.add(0);
                SensorState.put(i, listInt);

                List<Date> listDate = new ArrayList<>();
                listDate.add(Calendar.getInstance().getTime());
                SensorTime.put(i, listDate);
            }
            LoadData();
            isInit = true;
        }
    }

    public void OnChangeDevice() {
        isInit = false;

    }

    /**
     * Set data from temperature sensors
     *
     * @param result
     * @param <T>
     */

    public <T extends Cloneable> boolean SetDataTemp(TaskResponse<T> result) throws Exception {
        if (!isInit) {
            Log.e(this.getClass().getName(), "Before usage your need initilized");
            return false;
        }
        DeviceTempSensors tempSensor;

        if (UdpHelper.IS_TEST) {
            tempSensor = new DeviceTempSensors();
            ArrayList<Integer> temp = new ArrayList<>();
            for (int i = 0; i < 4; i++) {
                Random rand = new Random();
                temp.add(rand.nextInt(com.aerosoft.aquacontroller.Controller.DataHelper.Constants.MAX_TEMP_INDEX));
            }
            tempSensor.setSensors(temp);
        } else {
            try {
                tempSensor = (DeviceTempSensors) result.getResponse();
            } catch (Exception e) {
                throw new RuntimeException("Error data type");
            }
        }

        return SetDataTemp(tempSensor);

    }

    public boolean SetDataTemp(DeviceTempSensors tempSensor) throws Exception {
        if (!isInit) {
            Log.e(this.getClass().getName(), "Before usage your need initilized");
            return false;
        }
        if (tempSensor == null || SensorTime == null) return false;
        boolean isChanged = false;
        for (int i = 0; i < tempSensor.getSensors().size(); i++) {

            int indexSensorState = SensorState.get(i).size();
            int indexSensorTime = SensorTime.get(i).size();

            if (indexSensorState != indexSensorTime)
                throw new RuntimeException("Error data size");

            //TODO сделать логику для проверки цлостности данных
            if (SensorState.get(i).get(indexSensorState - 1) != tempSensor.getSensors().get(i)) {
                Date currentTime = Calendar.getInstance().getTime();
                Integer temp = tempSensor.getSensors().get(i);
                SensorState.get(i).add(temp);
                SensorTime.get(i).add(currentTime);
                isChanged = true;

            }
        }
        if (isChanged) {
            SaveData();
        }
        CheckToMaxSizeElements();
        return isChanged;
    }

    public int GetLastDataTemp(int sensor) {
        if (!isInit) {
            Log.e(this.getClass().getName(), "Before usage your need initilized");
            return -1;
        }
        if (SensorState.containsKey(sensor)) {
            int lastIndex = SensorState.get(sensor).size() - 1;
            return SensorState.get(sensor).get(lastIndex);
        }
        return -1;
    }

    public List<Date> GetXAxisValueForTemp(int sensor, int count) {
        List<Date> result = new ArrayList<>();
        if (!isInit) {
            Log.e(this.getClass().getName(), "Before usage your need initilized");
            return result;
        }
        Integer size = SensorTime.get(sensor).size();
        if (count > size) {
            count = size;
        }
        for (int i = 0; i < count; i++) {
            result.add(SensorTime.get(sensor).get(size - count + i));
        }
        return result;
    }

    private void SaveData() throws Exception {
        for (int i = 0; i < SensorState.size(); i++) {
            if (!Put(String.valueOf(UdpHelper.ADDRESS + KEY_SENSOR + i), SensorState.get(i)) ||
                    !Put(String.valueOf(UdpHelper.ADDRESS + KEY_DATE + i), SensorTime.get(i))) {
                throw new Exception("Data don't save!!!");
            }
        }

    }

    private void LoadData() {

        for (int j = 0; j <= maxTempSensor; j++) {
            List<Integer> sensor = Get(String.valueOf(UdpHelper.ADDRESS + KEY_SENSOR + j));
            List<Date> date = Get(String.valueOf(UdpHelper.ADDRESS + KEY_DATE + j));
            if (sensor != null)
                SensorState.put(j, sensor);
            if (date != null)
                SensorTime.put(j, date);
        }

    }

    private void CheckToMaxSizeElements() {
        for (int i = 0; i < maxTempSensor; i++) {
            while (SensorState.get(i).size() > MAX_SIZE_ARRAY || SensorTime.get(i).size() > MAX_SIZE_ARRAY) {
                SensorState.get(i).remove(0);
                SensorTime.get(i).remove(0);

            }
        }
    }

    private <T> boolean Put(String key, T value) {
        return Hawk.put(key, value);
    }

    private <T> T Get(String key) {
        return Hawk.get(key);
    }

    private long Count() {
        return Hawk.count();
    }

    private boolean Contains(String key) {
        return Hawk.contains(key);
    }

}
