package com.aerosoft.aquacontroller.View.Fragments.Preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.aerosoft.aquacontroller.Controller.DataHelper.Constants;
import com.aerosoft.aquacontroller.Controller.DataHelper.UdpHelper;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceCanalState;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.CHANAL_AUTO;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.CHANAL_INDEFINED;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.CHANAL_OFF;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.CHANAL_ON;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.CHANAL_TIMER_OFF;

/**
 * Created by Doc on 23.11.2017.
 */

public class ShareManager {

    private static ShareManager instance;
    private final Context context;
    private SharedPreferences preference;
    private boolean isInit = false;
    String[] canalNamesPref = new String[]{"pref_name_1", "pref_name_2", "pref_name_3", "pref_name_4", "pref_name_5", "pref_name_6", "pref_name_7", "pref_name_8"};

    public ShareManager(Context context) {
        this.context = context;

    }

    public static ShareManager getInstance(Context context) {
        if (instance == null) {
            instance = new ShareManager(context);
        }
        return instance;
    }

    public void Initilized() {
        if (isInit) return;
        isInit = true;
        preference = context.getSharedPreferences(UdpHelper.ADDRESS,
                Context.MODE_PRIVATE);
        SharedPreferences preferenceIp = context.getSharedPreferences(Constants.IP_PREF, Context.MODE_PRIVATE);
        if (!preferenceIp.getString("ip_address", "").isEmpty()) {
            UdpHelper.ADDRESS = preferenceIp.getString("ip_address", "");
        }
    }

    public Constants.STATE_CANAL_TYPE GetStateCanalOnType(DeviceCanalState deviceCanalState, String key) {
        int off = 0;
        int on = 0;
        HashSet<String> pref = (HashSet<String>) preference.getAll().get(key);
        if (pref == null || pref.size() == 0) {
            return Constants.STATE_CANAL_TYPE.CANAL_INDEFINED;
        }
        for (String p : pref) {
            if (deviceCanalState.getCanal() == null) continue;
            if (deviceCanalState.getCanal().get(Integer.parseInt(p) - 1) == CHANAL_TIMER_OFF) {
                off++;
            } else {
                on++;
            }
        }
        if (off > 0 && on > 0) {
            return Constants.STATE_CANAL_TYPE.CANAL_ON_OFF;
        }
        if (off > 0 && on == 0) {
            return Constants.STATE_CANAL_TYPE.CANAL_OFF;
        }
        if (off == 0 && on > 0) {
            return Constants.STATE_CANAL_TYPE.CANAL_ON;
        }
        return Constants.STATE_CANAL_TYPE.CANAL_INDEFINED;
    }

    public int GetHandStateCanalOnType(DeviceCanalState deviceCanalState, String key) {
        HashSet<String> pref = (HashSet<String>) preference.getAll().get(key);

        if (pref == null || pref.size() == 0) {
            return CHANAL_INDEFINED;
        }
        int off = 0;
        int on = 0;
        int auto = 0;
        for (String p : pref) {
            if (deviceCanalState.getCanal() == null) continue;
            if (deviceCanalState.getCanal_timer().get(Integer.parseInt(p) - 1) == CHANAL_OFF) {
                off++;
            } else if (deviceCanalState.getCanal_timer().get(Integer.parseInt(p) - 1) == CHANAL_ON) {
                on++;
            } else {
                auto++;
            }
        }
        if (off > on && off > auto) {
            return CHANAL_OFF;
        } else if (on > off && on > auto) {
            return CHANAL_ON;
        } else {
            return CHANAL_AUTO;
        }
    }

    @Nullable
    public List<Integer> GetCanalOnType(String key) {
        List<Integer> result = new ArrayList<>();

        HashSet<String> pref = (HashSet<String>) preference.getAll().get(key);
        if (pref == null || pref.size() == 0) {
            return null;
        }
        for (String p : pref) {
            result.add(Integer.parseInt(p) - 1);
        }
        return result;
    }

    public int GetPowerPreference(String key) {
        String val = preference.getString(key, "");
        try {
            return Integer.parseInt(val);
        } catch (Exception e) {
            return 0;
        }
    }

    public String GetPowerCanalName(int canal) {
        return preference.getString(canalNamesPref[canal], "");
    }

    public int GetTimeZone() {
        return Integer.parseInt(preference.getString("gmt_zone", ""));
    }

    public Set<String> GetAllIP() {
        SharedPreferences preferenceIp = context.getSharedPreferences(Constants.IP_PREF, Context.MODE_PRIVATE);
        Set<String> result = preferenceIp.getStringSet("all_ip_address", null);
        if (result == null) {
            result = new HashSet<>();
            if (preferenceIp.contains("ip_address")) {
                result.add(preferenceIp.getString("ip_address", ""));
            }
        }
        return result;
    }

    public void AddIPDevices(String ip) {
        SharedPreferences preferenceIp = context.getSharedPreferences(Constants.IP_PREF, Context.MODE_PRIVATE);
        Set<String> allIp = preferenceIp.getStringSet("all_ip_address", null);
        if (allIp == null) {
            allIp = new HashSet<String>();
        }
        if (!allIp.contains(ip) && ip.length() > 0) {
            allIp.add(ip);
            SharedPreferences.Editor prefEditor = preferenceIp.edit();
            prefEditor.putStringSet("all_ip_address", allIp);
            prefEditor.commit();
        }
    }

    public void RemoveIPDevices(String ip) {
        SharedPreferences preferenceIp = context.getSharedPreferences(Constants.IP_PREF, Context.MODE_PRIVATE);
        Set<String> allIp = preferenceIp.getStringSet("all_ip_address", null);
        if (allIp == null) {
            return;
        }
        if (allIp.contains(ip)) {
            allIp.remove(ip);
            SharedPreferences.Editor prefEditor = preferenceIp.edit();
            prefEditor.putStringSet("all_ip_address", allIp);
            prefEditor.commit();
        }
    }
}
