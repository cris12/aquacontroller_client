package com.aerosoft.aquacontroller.Controller.DataListener;

import com.aerosoft.aquacontroller.Model.DataModel.DeviceHoursTimer;

/**
 * Created by Vadim on 06.12.2016..
 * Interface for listener changed DataHoursTimer in List Adapter
 */

public interface IDataHoursTimerListener {
    void onEvent(DeviceHoursTimer e);
}
