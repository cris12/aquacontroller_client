package com.aerosoft.aquacontroller.Controller.DataHelper;

import java.util.regex.Pattern;

/**
 * Created by Doc on 25.11.2017.
 */

public class Constants {

    public static int ATTEMPT_COUNT = 10;
    public static int MAX_TEMP_INDEX = 76;
    public static String IP_PREF = "IP";
    public static final Pattern PATTERN = Pattern.compile(
            "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");
    public enum STATE_CANAL_TYPE  {
            CANAL_ON,
            CANAL_ON_OFF,
            CANAL_OFF,
            CANAL_INDEFINED
    }
}
