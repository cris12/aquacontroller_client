package com.aerosoft.aquacontroller.View.ViewAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aerosoft.aquacontroller.Controller.DataListener.IDataIPEventListener;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.View.Fragments.Preference.ShareManager;

import java.util.ArrayList;

public class ListIPAdapter extends RecyclerView.Adapter<ListIPAdapter.ListIPAdapterHolder> {

    private ShareManager shareManager;
    private ArrayList<String> data;
    private IDataIPEventListener listener;
    public ListIPAdapter(Context context, IDataIPEventListener listener) {
        shareManager = ShareManager.getInstance(context);
        data = new ArrayList<>(shareManager.GetAllIP());
        this.listener = listener;
    }

    @Override
    public ListIPAdapter.ListIPAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.iplist_item, parent, false);
        ListIPAdapterHolder vh = new ListIPAdapterHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ListIPAdapter.ListIPAdapterHolder holder, final int position) {
        holder.mTextView.setText(data.get(position));
        holder.ipDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareManager.RemoveIPDevices(data.get(position));
                listener.Event();
            }
        });
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.Event(data.get(position));
            }
        });
        holder.layout.setOnLongClickListener( new View.OnLongClickListener(){

            @Override
            public boolean onLongClick(View v) {
                holder.ipDeleteButton.setVisibility(View.VISIBLE);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ListIPAdapterHolder extends RecyclerView.ViewHolder {

        public TextView mTextView;
        public ImageButton ipDeleteButton;
        public LinearLayout layout;

        public ListIPAdapterHolder(View itemView) {
            super(itemView);

            ipDeleteButton = itemView.findViewById(R.id.ip_delete);
            mTextView = itemView.findViewById(R.id.ip_text_view);
            layout =  itemView.findViewById(R.id.device_layout);
            ipDeleteButton.setVisibility(View.INVISIBLE);

        }
    }
}
