package com.aerosoft.aquacontroller.View.Fragments.timers;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aerosoft.aquacontroller.Controller.DataListener.IRequestListener;
import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.aerosoft.aquacontroller.Controller.DataProvider.RequestManager;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceSecondsTimer;
import com.aerosoft.aquacontroller.Controller.DataListener.IDataSecondsTimerListener;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.View.DeviceActivity;
import com.aerosoft.aquacontroller.View.ViewAdapter.TimerSecondListAdapter;
import com.aerosoft.aquacontroller.View.ViewHelper.ToastHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.EventObject;

import static android.view.View.INVISIBLE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DATA_DEVICE_BUSY;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TimersSecondFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TimersSecondFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TimersSecondFragment extends Fragment implements IRequestListener {
    RecyclerView mRecyclerView;
    FloatingActionButton fab;
    private DeviceSecondsTimer event;
    private Drawable mDivider;


    private OnFragmentInteractionListener mListener;
    private DeviceInfo deviceInfo;
    private DeviceSecondsTimer changedDeviceSecondsTimer;
    int attemptCount = com.aerosoft.aquacontroller.Controller.DataHelper.Constants.ATTEMPT_COUNT;
    private Snackbar snackbar;

    public TimersSecondFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TimersSecondFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TimersSecondFragment newInstance(String param1, String param2) {
        TimersSecondFragment fragment = new TimersSecondFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timers_second, container, false);
        fab = view.findViewById(R.id.floatingActionButtonSecond);
        mDivider = getResources().getDrawable(R.drawable.line_divider);
        mRecyclerView = view.findViewById(R.id.recycler_view_second);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!((DeviceActivity) getActivity()).isWait) {
                    ((DeviceActivity) getActivity()).isWait = true;

                    snackbar = Snackbar.make(v, getString(R.string.SendDataMessage), Snackbar.LENGTH_INDEFINITE)
                            .setAction("Action", null);
                    snackbar.show();
                    fab.setVisibility(INVISIBLE);
                    if (changedDeviceSecondsTimer != null) {
                        new RequestManager().SendPOSTRequest(ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_SECOND_TIMER_SATE, changedDeviceSecondsTimer, TimersSecondFragment.this);

                    } else {
                        ToastHelper.ToastDeviceLog(getActivity(), RESULT_DATA_DEVICE_BUSY);
                    }
                }
            }
        });
        return view;
    }

    @Override
    public void onEvent(EventObject e) {

        if (e == null || e.getSource() != ProtocolHelper.TaskResult.RESULT_SUCCESS || !onCheckValidResult()) {
            Log.e(getClass().getName(),"Invalid result Second Timer State");
            attemptCount--;
            if (attemptCount > 0)
                new RequestManager().SendPOSTRequest(ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_SECOND_TIMER_SATE, changedDeviceSecondsTimer, TimersSecondFragment.this);
            else {
                if (getActivity() != null)
                    if (getActivity() != null && !((DeviceActivity) getActivity()).isWait)
                ((DeviceActivity) getActivity()).isWait = false;
                fab.setVisibility(View.VISIBLE);
                ToastHelper.ToastDeviceLog(e);
            }
        } else {

            if (getActivity() != null)
            ((DeviceActivity) getActivity()).isWait = false;
            ToastHelper.ToastDeviceLog(e);
            changedDeviceSecondsTimer = null;
        }
    }

    private boolean onCheckValidResult() {
        if (!this.event.getSecond_timer_canal().equals(changedDeviceSecondsTimer.getSecond_timer_canal()))
            return false;
        if (!this.event.getSecond_timer_duration().equals(changedDeviceSecondsTimer.getSecond_timer_duration()))
            return false;
        if (!this.event.getSecond_timer_hour_start().equals(changedDeviceSecondsTimer.getSecond_timer_hour_start()))
            return false;
        if (!this.event.getSecond_timer_min_start().equals(changedDeviceSecondsTimer.getSecond_timer_min_start()))
            return false;
        return this.event.getSecond_timer_state().equals(changedDeviceSecondsTimer.getSecond_timer_state());
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        changedDeviceSecondsTimer = null;
        fab.setVisibility(INVISIBLE);
    }

    @Override
    public void onStart() {
        super.onStart();
        fab.setVisibility(INVISIBLE);
        EventBus.getDefault().register(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceInfo deviceInfo) {
        if (deviceInfo != null) {
            this.deviceInfo = deviceInfo;
            if (this.event != null) {
                if (changedDeviceSecondsTimer == null) {
                    CreateListAdapter();
                }
            }
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceSecondsTimer event) {
        if (event != null) {
            this.event = event;
            if (this.deviceInfo != null) {
                if (changedDeviceSecondsTimer == null) {
                    CreateListAdapter();
                }
            }
        }
    }

    private void CreateListAdapter() {
        if (snackbar != null)
            snackbar.dismiss();
        mRecyclerView.setAdapter(new TimerSecondListAdapter(getContext(), event, this.deviceInfo, new IDataSecondsTimerListener() {
            @Override
            public void onEvent(DeviceSecondsTimer e) {
                fab.setVisibility(View.VISIBLE);
                changedDeviceSecondsTimer = e;
            }
        }));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in thatё
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
