package com.aerosoft.aquacontroller.View.ViewAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.aerosoft.aquacontroller.Controller.Chart.ChartController;
import com.aerosoft.aquacontroller.Model.DataModel.ManagerStatsSensor;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.Controller.Chart.HelperLineChartView;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;

import java.util.List;

/**
 * Created by Doc on 14.10.2017.
 */

public class ChartTempAdapter extends ArrayAdapter {

    private final Context context;
    private final List<Integer> indexTempSensor;
    private List<List<Entry>> data;
    private final int[] ColorFade = {R.drawable.fade_layout, R.drawable.fade_list_green, R.drawable.fade_list_yellow, R.drawable.fade_list_red};
    public ChartTempAdapter(Context context, List<List<Entry>> data, List<Integer> indexTempSensor) {
        super(context, 0, data);
        this.data = data;
        this.context = context;
        this.indexTempSensor = indexTempSensor;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.fragment_temperatura_item, null);
            holder.chart = convertView.findViewById(R.id.lineStateTempChart);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        HelperLineChartView.Builder builder = new HelperLineChartView.Builder(new LineChart[]{holder.chart}, HelperLineChartView.ChartView.CHART_VIEW_LIST, context);
        try {
            builder
                    .SetChatLineDataSet(new List[]{data.get(position)}, new int[]{GetColor(position)}, position + 1)
                    .SetChatView()
                    .SetXAxis(ManagerStatsSensor.getInstance(context).GetXAxisValueForTemp(indexTempSensor.get(position), data.get(position).size()))
                    .SetYAxis()
                    .Build();

        } catch (Exception e) {
            e.printStackTrace();
        }


        return convertView;
    }

    private class ViewHolder {

        LineChart chart;
    }

    private int GetColor(int position){
        if (position  == 0) return ColorFade[0];
        if (position % 4 == 0) return ColorFade[3];
        if (position % 3 == 0) return ColorFade[2];
        if (position % 2 == 0) return ColorFade[1];
        return ColorFade[1];
    }

}
