package com.aerosoft.aquacontroller.View.ViewAdapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.aerosoft.aquacontroller.Model.DataModel.DeviceHoursTimer;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Controller.DataListener.IDataHoursTimerListener;
import com.aerosoft.aquacontroller.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Vadim on 01.12.2016.
 */

public class TimerHoursListAdapter extends RecyclerView.Adapter<TimerHoursListAdapter.TimerViewHolder> {


    private DeviceInfo deviceInfo;
    private final IDataHoursTimerListener eventListner;
    private DeviceHoursTimer timers;
    private Context mContext;
    private final int[] imagesID = {R.drawable.ic_numeric_1_box, R.drawable.ic_numeric_2_box, R.drawable.ic_numeric_3_box, R.drawable.ic_numeric_4_box, R.drawable.ic_numeric_5_box, R.drawable.ic_numeric_6_box, R.drawable.ic_numeric_7_box, R.drawable.ic_numeric_8_box};
    private boolean isSpinnerTouched;

    public TimerHoursListAdapter(Context mContext, DeviceHoursTimer timers, DeviceInfo deviceInfo, IDataHoursTimerListener eventListner) {
        this.timers = timers;
        this.mContext = mContext;
        this.deviceInfo = deviceInfo;
        this.eventListner = eventListner;
    }


    @Override
    public TimerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        return new TimerViewHolder(inflater.inflate(R.layout.timer_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final TimerViewHolder holder, final int position) {
        SpinnerAdapter customAdapter = new SpinnerAdapter(mContext, imagesID, deviceInfo.GetMaxCanal());
        holder.spChannel.setAdapter(customAdapter);
        holder.spChannel.setSelection(timers.getHours_timer_canal().get(position));
        holder.spChannel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                isSpinnerTouched = true;
                return false;
            }
        });
        holder.spChannel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                if (timers.getHours_timer_canal().get(position) != pos) {
                    timers.getHours_timer_canal().set(position, pos);
                    eventListner.onEvent(timers);
                    Log.d(getClass().getName().toString(), timers.getHours_timer_canal().toString());
                    if (isSpinnerTouched) {
                        isSpinnerTouched = false;

                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (holder.controlsView.getVisibility() == View.GONE) {
            holder.chanalImage.setImageResource(imagesID[timers.getHours_timer_canal().get(position)]);
        } else {
            holder.chanalImage.setImageResource(R.drawable.ic_up_arrow_angle);
        }

        holder.timerStart.setText(String.format("%02d", timers.getHours_timer_min_start().get(position)) +" "+ mContext.getString(R.string.min_item_list));
        holder.timerEnd.setText(String.format("%02d", timers.getHours_timer_min_stop().get(position)) +" "+ mContext.getString(R.string.min_item_list));
        holder.timerStartChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog d = new Dialog(mContext);
                d.requestWindowFeature(Window.FEATURE_NO_TITLE);
                d.setContentView(R.layout.number_dialog);
                ImageButton minus = d.findViewById(R.id.btnMinus);
                ImageButton plus = d.findViewById(R.id.btnPlus);
                final EditText tempText = d.findViewById(R.id.editTemp);
                tempText.setEnabled(false);
                final Button b1 = d.findViewById(R.id.button1);
                b1.setText(mContext.getString(R.string.button_cancel));
                ImageView imageTitle = d.findViewById(R.id.imageView12);
                imageTitle.setImageResource(R.mipmap.time_icon_1);
                TextView title = d.findViewById(R.id.dialog_title);
                title.setText(mContext.getString(R.string.dialog_title_min_start));
                tempText.setText(timers.getHours_timer_min_start().get(position).toString());

                minus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        b1.setText(mContext.getString(R.string.button_ok));
                        Integer val = timers.getHours_timer_min_start().get(position);
                        if (val > 0) {
                            val--;
                            timers.getHours_timer_min_start().set(position, val);
                            tempText.setText(timers.getHours_timer_min_start().get(position).toString());
                        }
                    }
                });

                plus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        b1.setText(mContext.getString(R.string.button_ok));
                        Integer val = timers.getHours_timer_min_start().get(position);
                        if (val < 59) {
                            if (59 - val >= 10)
                                val += 10;
                            else
                                val += 59 - val;
                            timers.getHours_timer_min_start().set(position, val);
                            tempText.setText(timers.getHours_timer_min_start().get(position).toString());
                        }
                    }
                });

                b1.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v) {
                        holder.timerStart.setText(String.format("%02d", timers.getHours_timer_min_start().get(position)) +" "+ mContext.getString(R.string.min_item_list));
                        Log.d(getClass().getName().toString(), timers.getHours_timer_min_start().toString());
                        eventListner.onEvent(timers);
                        d.dismiss();

                    }
                });

                d.show();
            }
        });
        holder.timerEndChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog d = new Dialog(mContext);
                d.requestWindowFeature(Window.FEATURE_NO_TITLE);
                d.setContentView(R.layout.number_dialog);
                ImageButton minus = d.findViewById(R.id.btnMinus);
                ImageButton plus = d.findViewById(R.id.btnPlus);
                final EditText tempText = d.findViewById(R.id.editTemp);
                tempText.setEnabled(false);
                final Button b1 = d.findViewById(R.id.button1);
                b1.setText(mContext.getString(R.string.button_cancel));
                ImageView imageTitle = d.findViewById(R.id.imageView12);
                imageTitle.setImageResource(R.mipmap.time_icon_6);
                TextView title = d.findViewById(R.id.dialog_title);
                title.setText(mContext.getString(R.string.dialog_title_min_end));
                tempText.setText(timers.getHours_timer_min_stop().get(position).toString());

                minus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        b1.setText(mContext.getString(R.string.button_ok));
                        Integer val = timers.getHours_timer_min_stop().get(position);
                        if (val > 0) {
                            val--;
                            timers.getHours_timer_min_start().set(position, val);
                            tempText.setText(timers.getHours_timer_min_stop().get(position).toString());
                        }
                    }
                });

                plus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        b1.setText(mContext.getString(R.string.button_ok));
                        Integer val = timers.getHours_timer_min_stop().get(position);
                        if (val < 59) {
                            if (59 - val >= 10)
                                val += 10;
                            else
                                val += 59 - val;
                            timers.getHours_timer_min_stop().set(position, val);
                            tempText.setText(timers.getHours_timer_min_stop().get(position).toString());
                        }
                    }
                });

                b1.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v) {
                        holder.timerEnd.setText(String.format("%02d", timers.getHours_timer_min_stop().get(position)) +" "+ mContext.getString(R.string.min_item_list));
                        Log.d(getClass().getName().toString(), timers.getHours_timer_min_stop().toString());
                        eventListner.onEvent(timers);
                        d.dismiss();

                    }
                });

                d.show();
            }
        });
        holder.switchTimer.setChecked(timers.getHours_timer_state().get(position) == 1);
        holder.switchTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timers.getHours_timer_state().get(position) == 0)
                    timers.getHours_timer_state().set(position, 1);
                else
                    timers.getHours_timer_state().set(position, 0);
                if (timers.getHours_timer_state().get(position) == 0) {
                    holder.timerEnable.setText(mContext.getString(R.string.OFF));
                } else {
                    holder.timerEnable.setText(mContext.getString(R.string.ON));
                }
                Log.d(getClass().getName().toString(), timers.getHours_timer_state().toString());
                eventListner.onEvent(timers);

            }
        });
        if (timers.getHours_timer_state().get(position) == 0) {
            holder.timerEnable.setText(mContext.getString(R.string.OFF));
        } else {
            holder.timerEnable.setText(mContext.getString(R.string.ON));
        }
    }


    @Override
    public int getItemCount() {
        return deviceInfo.getMax_timer();
    }

    class TimerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.controls_view)
        View controlsView;

        //group
        @BindView(R.id.group_timer_daily_chanal)
        ImageView chanalImage;
        @BindView(R.id.group_timer_daily_start)
        TextView timerStart;
        @BindView(R.id.group_timer_daily_end)
        TextView timerEnd;
        @BindView(R.id.group_timer_daily_enable)
        TextView timerEnable;
        //child
        @BindView(R.id.child_timer_start)
        ImageButton timerStartChild;
        @BindView(R.id.child_timer_end)
        ImageButton timerEndChild;
        @BindView(R.id.child_timer_swith)
        SwitchCompat switchTimer;

        @BindView(R.id.spinner_chanal_timer)
        Spinner spChannel;

        TimerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.timer_view)
        void onTimerViewClicked(View timerView) {
            if (controlsView.getVisibility() == View.VISIBLE) {
                notifyDataSetChanged();
                controlsView.setVisibility(View.GONE);
            } else {
                controlsView.setVisibility(View.VISIBLE);
            }
            if (controlsView.getVisibility() != View.GONE) {
                chanalImage.setImageResource(R.drawable.ic_up_arrow_angle);
            }

        }

    }
}
