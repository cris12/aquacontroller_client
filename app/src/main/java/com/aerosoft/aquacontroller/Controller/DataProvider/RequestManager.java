package com.aerosoft.aquacontroller.Controller.DataProvider;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.aerosoft.aquacontroller.Controller.DataHelper.UdpHelper;
import com.aerosoft.aquacontroller.Controller.DataListener.IRequestListener;
import com.aerosoft.aquacontroller.Controller.DataTest.RequestTestHelper;
import com.aerosoft.aquacontroller.Model.TaskRequest.TaskGetRequest;
import com.aerosoft.aquacontroller.Model.TaskRequest.TaskPostRequest;
import com.aerosoft.aquacontroller.Model.TaskRequest.TaskRequest;
import com.aerosoft.aquacontroller.Model.TaskResponse.TaskResponse;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.EventObject;

/**
 * Created by Vadim on 15.11.2016.
 */

public class RequestManager {

    private UdpClientGetThread threadRequest;
    private IRequestListener listener;

    public void SendGETRequest(final ProtocolHelper.REQUEST_TYPE request_type, final IRequestListener listener) {
        this.listener = listener;
        if (RequestTestHelper.IsTestRequest(request_type, listener))
            return;
        SendRequest(new TaskGetRequest(request_type, null));
    }

    public void SendPOSTRequest(ProtocolHelper.REQUEST_TYPE request_type, Object jsonObject, IRequestListener listener) {
        this.listener = listener;
        SendRequest(new TaskPostRequest(request_type, jsonObject));
    }

    private void SendRequest(TaskRequest taskRequest) {
        if (threadRequest == null) {
            threadRequest = new UdpClientGetThread(taskRequest);
            threadRequest.start();
        }
    }

    /**
     * Class for send UDP request on device
     */
    private class UdpClientGetThread extends Thread {

        private boolean running = true;
        private TaskRequest taskRequest;
        private DatagramSocket socket;
        private InetAddress address;
        final Handler myHandler = new Handler(Looper.getMainLooper());
        private ProtocolHelper.TaskResult result = ProtocolHelper.TaskResult.RESULT_SUCCESS;
        private String json = "";


        public UdpClientGetThread(TaskRequest taskRequest) {
            this.taskRequest = taskRequest;
        }

        public boolean isRunning() {
            return running;
        }

        @Override
        public void run() {
            running = true;
            try {
                socket = new DatagramSocket();
                address = InetAddress.getByName(UdpHelper.ADDRESS);

                // send request
                byte[] buf = new byte[UdpHelper.MAX_UDP_DATAGRAM_LEN];
                Log.d(this.getClass().getName(), this.taskRequest.GetJSONRequest());
                DatagramPacket packet = UdpHelper.stringToPacket(this.taskRequest.GetJSONRequest(), new DatagramPacket(buf, buf.length, address, UdpHelper.PORT));
                socket.send(packet);
                socket.setSoTimeout(UdpHelper.MAX_UDP_TIMEOUT);
                // get response
                packet = new DatagramPacket(buf, buf.length);
                while (true) {        // receive data until timeout
                    try {
                        socket.receive(packet);
                        if (packet != null && packet.getAddress().getHostAddress().equals(UdpHelper.ADDRESS)) {
                            String message = UdpHelper.stringFromPacket(packet);
                            Log.d(this.getClass().getName(), message);
                            json = message;
                            if (!json.contains(UdpHelper.RESPONSE_ERROR)) {
                                result = ProtocolHelper.TaskResult.RESULT_SUCCESS;
                            } else {
                                result = ProtocolHelper.TaskResult.RESULT_ERROR_NOT_RESPONSE;
                            }
                            myHandler.post(ActionEventListener);
                        }
                        return;
                    } catch (SocketTimeoutException e) {
                        result = ProtocolHelper.TaskResult.RESULT_ERROR_SOCKET_TIMEOUT_EXCEPTION;
                        myHandler.post(ActionEventListener);
                        running = false;
                        return;
                    }
                }
            } catch (SocketException e) {
                result = ProtocolHelper.TaskResult.RESULT_ERROR_SOCKET_EXCEPTION;
                e.printStackTrace();
                myHandler.post(ActionEventListener);
            } catch (UnknownHostException e) {
                result = ProtocolHelper.TaskResult.RESULT_ERROR_UNKNOWN_HOST_EXCEPTION;
                myHandler.post(ActionEventListener);
                e.printStackTrace();
            } catch (IOException e) {
                result = ProtocolHelper.TaskResult.RESULT_ERROR_IO_EXCEPTION;
                myHandler.post(ActionEventListener);
                e.printStackTrace();
            } finally {
                if (socket != null) {
                    socket.close();
                }
                running = false;
            }

        }

        public String getLastMessage() {
            return json;
        }

        public ProtocolHelper.TaskResult getResult() {
            return result;
        }

        private Runnable ActionEventListener = new Runnable() {

            public void run() {

                if (getResult() == ProtocolHelper.TaskResult.RESULT_SUCCESS) {
                    TaskResponse<Cloneable> taskResult = taskRequest.TaskRequestComplete(getLastMessage());
                    if (taskResult != null) {
                        EventBus.getDefault().postSticky(taskResult.getResponse());
                        if (listener != null) {
                            listener.onEvent(new EventObject(getResult()));
                        }
                    } else {
                        if (listener != null) {
                            listener.onEvent(new EventObject(ProtocolHelper.TaskResult.RESULT_DATA_CORRUPTED));
                        }
                    }
                } else {
                    listener.onEvent(new EventObject(getResult()));
                }
                threadRequest = null;
            }
        };
    }

}
