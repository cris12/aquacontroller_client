package com.aerosoft.aquacontroller.Controller.Chart;


import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.aerosoft.aquacontroller.R;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by DrJar on 15.09.2017.
 */

public class HelperLineChartView {
    public enum ChartView {CHART_VIEW_LAYOUT, CHART_VIEW_BUTTON, CHART_VIEW_LIST}

    private HelperLineChartView() {
    }

    public static class Builder {
        private final ChartView type;
        private final Context context;
        private LineChart[] chart;

        public Builder(LineChart[] chart, ChartView type, Context context) {
            this.chart = chart;
            this.type = type;
            this.context = context;
        }

        public Builder SetChatLineDataSet(List<Entry>[] entryList, int[] drawableColor, int index) {
            LineDataSet set1;
            for (int i = 0; i < chart.length; i++) {
                if (chart[i].getData() != null && chart[i].getData().getDataSetCount() > 0) {
                    set1 = (LineDataSet) chart[i].getData().getDataSetByIndex(0);
                    set1.setValues(entryList[i]);
                    chart[i].getData().notifyDataChanged();
                    chart[i].notifyDataSetChanged();
                } else {
                    String label = "";
                    switch (type) {
                        case CHART_VIEW_LAYOUT:
                            break;
                        case CHART_VIEW_BUTTON:
                            break;
                        case CHART_VIEW_LIST:
                            label = context.getString(R.string.deviceTempSensor) + " " + index;
                            break;
                    }
                    set1 = new LineDataSet(entryList[i], label);
                    set1.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
                    set1.setCubicIntensity(0.05f);
                    set1.setDrawFilled(true);

                    if (Utils.getSDKInt() >= 18) {
                        Drawable drawable = ContextCompat.getDrawable(context, drawableColor[i]);
                        set1.setFillDrawable(drawable);
                    } else {
                        set1.setFillColor(Color.BLACK);
                    }
                    set1.setFillFormatter(new IFillFormatter() {
                        @Override
                        public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                            return -10;
                        }
                    });
                    set1.setDrawCircles(false);
                    switch (type) {
                        case CHART_VIEW_LIST:
                            set1.setCircleRadius(4f);
                            set1.setLineWidth(1.8f);
                            set1.setColor(Color.argb(225, 7, 92, 107));
                            set1.setCircleColor(Color.argb(225, 7, 92, 107));
                            set1.setValueFormatter(new MyValueFormatter());
                        case CHART_VIEW_LAYOUT:
                            set1.setCircleRadius(2f);
                            set1.setLineWidth(1.8f);
                            set1.setColor(Color.argb(225, 7, 92, 107));
                            set1.setCircleColor(Color.argb(225, 7, 92, 107));
                            break;
                        case CHART_VIEW_BUTTON:
                            set1.setDrawHorizontalHighlightIndicator(false);
                            set1.setDrawVerticalHighlightIndicator(false);
                            set1.setCircleRadius(2f);
                            set1.setLineWidth(0.9f);
                            set1.setColor(Color.argb(225, 7, 92, 107));
                            set1.setCircleColor(Color.argb(225, 7, 92, 107));
                            set1.setDrawCircles(false);
                            set1.setCircleRadius(4f);

                            break;

                    }

                    LineData data = new LineData(set1);
                    data.setDrawValues(false);
                    switch (type) {

                        case CHART_VIEW_LAYOUT:
                            data.setValueTypeface(Typeface.DEFAULT);
                            data.setValueTextSize(9f);
                            break;
                        case CHART_VIEW_BUTTON:
                            break;
                    }

                    chart[i].setData(data);
                }
            }
            return this;
        }

        public Builder SetChatView() {
            for (int i = 0; i < chart.length; i++) {
                chart[i].setScaleEnabled(false);
                chart[i].setPinchZoom(false);
                chart[i].setNoDataText("");
                chart[i].getLegend().setEnabled(false);
                chart[i].getLegend().setPosition(Legend.LegendPosition.ABOVE_CHART_RIGHT);
                chart[i].getLegend().setYOffset(1f);
                chart[i].getLegend().setTextColor(Color.argb(225, 7, 92, 107));
                chart[i].setViewPortOffsets(0, 0, 0, 0);
                chart[i].getDescription().setEnabled(false);
                switch (type) {
                    case CHART_VIEW_LAYOUT:
                        chart[i].animateXY(1000, 1000);
                        chart[i].setDragEnabled(true);
                        chart[i].setTouchEnabled(true);
                        break;
                    case CHART_VIEW_BUTTON:
                        chart[i].getAxisLeft().setDrawGridLines(false);
                        chart[i].getAxisRight().setDrawGridLines(false);
                        chart[i].animateXY(0, 0);
                        chart[i].setTouchEnabled(false);
                        chart[i].setDragEnabled(false);
                        break;
                    case CHART_VIEW_LIST:
                        chart[i].setScaleEnabled(true);
                        chart[i].getLegend().setEnabled(true);
                        chart[i].getDescription().setEnabled(false);
                        chart[i].setViewPortOffsets(100, 50, 50, 80);
                        chart[i].animateXY(0, 0);
                        chart[i].setDragEnabled(true);
                        chart[i].setTouchEnabled(true);
                        break;
                }
            }
            return this;
        }

        public Builder SetXAxis() {
            return SetXAxis(null);
        }

        public Builder SetXAxis(List<Date> values) {
            for (int i = 0; i < chart.length; i++) {
                XAxis x = chart[i].getXAxis();
               // x.setXOffset(1f);

                x.setGridColor(Color.argb(225, 7, 92, 107));
                x.setAxisLineColor(Color.argb(225, 7, 92, 107));
                x.setTextColor(Color.argb(225, 7, 92, 107));
                x.setTypeface(Typeface.DEFAULT);
                if(values != null){
                    x.setValueFormatter(new MyXAxisValueFormatter(values));
                }
                switch (type) {
                    case CHART_VIEW_LAYOUT:
                        x.setPosition(XAxis.XAxisPosition.BOTTOM_INSIDE);
                        x.setLabelCount(12, false);
                        x.setDrawAxisLine(true);
                        x.setDrawGridLines(true);
                        break;
                    case CHART_VIEW_BUTTON:
                        x.setEnabled(false);
                        x.setDrawGridLines(false);
                        break;
                    case CHART_VIEW_LIST:
                        x.setLabelCount(24, false);
                        x.setDrawAxisLine(true);
                        x.setDrawGridLines(true);
                        x.setPosition(XAxis.XAxisPosition.BOTTOM);
                        x.setGranularity(2f);
                        x.setTextSize(8f);

                        break;
                }
            }
            return this;
        }

        public Builder SetYAxis() {
            for (int i = 0; i < chart.length; i++) {
                YAxis y = chart[i].getAxisLeft();
                y.setGridColor(Color.argb(225, 7, 92, 107));
                y.setAxisLineColor(Color.argb(225, 7, 92, 107));
                y.setTextColor(Color.argb(225, 7, 92, 107));

                switch (type) {
                    case CHART_VIEW_LAYOUT:
                        y.setTypeface(Typeface.DEFAULT);
                        y.setLabelCount(6, false);
                        y.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
                        y.setDrawGridLines(false);
                        break;
                    case CHART_VIEW_LIST:
                        y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
                        chart[i].getAxisRight().setEnabled(false);
                        break;
                    case CHART_VIEW_BUTTON:
                        y.setEnabled(false);
                        y.setDrawGridLines(false);

                        break;
                }
            }
            return this;
        }

        public HelperLineChartView Build() {
            for (int i = 0; i < chart.length; i++) {
                chart[i].invalidate();
            }
            return new HelperLineChartView();
        }
    }


    public static class MyValueFormatter implements IValueFormatter {

        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            // write your logic here
            return String.valueOf(value) + "C°"; // e.g. append a dollar-sign
        }
    }

    public static class MyXAxisValueFormatter implements IAxisValueFormatter {
        private List<Date> mValues;
        public MyXAxisValueFormatter(List<Date> values) {
            this.mValues = values;
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            // "value" represents the position of the label on the axis (x or y)
            DateFormat dateFormat = new SimpleDateFormat("HH:mm");
            String result = "";
            try{ result = dateFormat.format(mValues.get((int) value));}catch (Exception e){

            }
            return result;
        }


    }


}
