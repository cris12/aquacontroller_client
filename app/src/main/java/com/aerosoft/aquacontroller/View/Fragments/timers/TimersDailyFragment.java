package com.aerosoft.aquacontroller.View.Fragments.timers;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aerosoft.aquacontroller.Controller.DataListener.IRequestListener;
import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.aerosoft.aquacontroller.Controller.DataProvider.RequestManager;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTimer;
import com.aerosoft.aquacontroller.Controller.DataListener.IDataDailyTimerListener;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.View.DeviceActivity;
import com.aerosoft.aquacontroller.View.ViewAdapter.TimerDailyListAdapter;
import com.aerosoft.aquacontroller.View.ViewHelper.ToastHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.EventObject;

import static android.view.View.INVISIBLE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.TaskResult.RESULT_DATA_DEVICE_BUSY;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TimersDailyFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TimersDailyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TimersDailyFragment extends Fragment implements IRequestListener {

    private OnFragmentInteractionListener mListener;
    RecyclerView mRecyclerView;
    FloatingActionButton fab;
    private DeviceInfo deviceInfo;
    private DeviceTimer event;
    private Drawable mDivider;
    private DeviceTimer changedDeviceTimer;
    int attemptCount = com.aerosoft.aquacontroller.Controller.DataHelper.Constants.ATTEMPT_COUNT;
    private Snackbar snackbar;

    public TimersDailyFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TimersFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TimersDailyFragment newInstance() {
        TimersDailyFragment fragment = new TimersDailyFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timers_daily, container, false);
        fab = view.findViewById(R.id.floatingActionButtonDaily);
        mDivider = getResources().getDrawable(R.drawable.line_divider);
        mRecyclerView = view.findViewById(R.id.recycler_view_daily);
        //  mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), null));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!((DeviceActivity) getActivity()).isWait) {
                    ((DeviceActivity) getActivity()).isWait = true;
                    snackbar = Snackbar.make(v, getString(R.string.SendDataMessage), Snackbar.LENGTH_INDEFINITE)
                            .setAction("Action", null);
                    snackbar.show();
                    fab.setVisibility(INVISIBLE);
                    if (changedDeviceTimer != null) {
                        new RequestManager().SendPOSTRequest(ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_DAILY_TIMER_SATE, changedDeviceTimer, TimersDailyFragment.this);

                    }
                } else {
                    ToastHelper.ToastDeviceLog(getActivity(), RESULT_DATA_DEVICE_BUSY);
                }
            }
        });
        return view;
    }

    @Override
    public void onEvent(EventObject e) {

        if (e == null || e.getSource() != ProtocolHelper.TaskResult.RESULT_SUCCESS || !onCheckValidResult()) {
            Log.e(getClass().getName(),"Invalid result Daily Timer State");
            attemptCount--;
            if (attemptCount > 0)
                new RequestManager().SendPOSTRequest(ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_DAILY_TIMER_SATE, changedDeviceTimer, TimersDailyFragment.this);
            else {
                fab.setVisibility(View.VISIBLE);
                ToastHelper.ToastDeviceLog(e);
                if (getActivity() != null)
                ((DeviceActivity) getActivity()).isWait = false;
            }
        } else {
            try {
                CreateListAdapter();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            if (getActivity() != null)
            ((DeviceActivity) getActivity()).isWait = false;
            ToastHelper.ToastDeviceLog(e);
            changedDeviceTimer = null;
        }
    }

    private boolean onCheckValidResult() {
        if (!this.event.getDaily_timer_canal().equals(changedDeviceTimer.getDaily_timer_canal()))
            return false;
        if (!this.event.getDaily_timer_hour_end().equals(changedDeviceTimer.getDaily_timer_hour_end()))
            return false;
        if (!this.event.getDaily_timer_hour_start().equals(changedDeviceTimer.getDaily_timer_hour_start()))
            return false;
        if (!this.event.getDaily_timer_min_end().equals(changedDeviceTimer.getDaily_timer_min_end()))
            return false;
        if (!this.event.getDaily_timer_min_start().equals(changedDeviceTimer.getDaily_timer_min_start()))
            return false;
        return this.event.getDaily_timer_state().equals(changedDeviceTimer.getDaily_timer_state());
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        changedDeviceTimer = null;
        fab.setVisibility(INVISIBLE);
    }

    @Override
    public void onStart() {
        super.onStart();
        fab.setVisibility(INVISIBLE);
        EventBus.getDefault().register(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceInfo deviceInfo) {
        if (deviceInfo != null) {
            this.deviceInfo = deviceInfo;
            if (this.event != null) {
                if (changedDeviceTimer == null)
                    CreateListAdapter();
            }
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceTimer event) {
        if (event != null) {
            this.event = event;
            if (this.deviceInfo != null)
                if (changedDeviceTimer == null)
                    CreateListAdapter();
        }
    }

    private void CreateListAdapter() {
        if (snackbar != null)
            snackbar.dismiss();
        mRecyclerView.setAdapter(new TimerDailyListAdapter(getContext(), event, this.deviceInfo, new IDataDailyTimerListener() {
            @Override
            public void onEvent(DeviceTimer e) {
                changedDeviceTimer = e;
                if (getActivity() != null && !((DeviceActivity) getActivity()).isWait)
                fab.setVisibility(View.VISIBLE);
            }
        }));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
