package com.aerosoft.aquacontroller.Model.TaskRequest;

import android.util.Log;

import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.google.gson.Gson;

import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.PROTOCOL.POST;

/**
 * Created by Vadim on 30.01.2017.
 */

public class TaskPostRequest extends TaskRequest {

    private static final ProtocolHelper.PROTOCOL protocol = POST;

    public TaskPostRequest(ProtocolHelper.REQUEST_TYPE request_type, Object jsonObject) {
        super(request_type, jsonObject,  protocol);
    }

    @Override
    public void InitializedRequest() {
        request = "{\"status\":\"" + ProtocolHelper.ProtocolList.get(protocol) + "\",\"message\":\"";
        request += ProtocolHelper.CommandList.get(request_type);
        String json = "\",\"data\": ";
        try {
            json += new Gson().toJson(jsonObject) + "}";

        } catch (Exception e) {
            Log.e(getClass().getName().toString(), e.getStackTrace().toString());
            json += "{}}";
        }
        request += json;
    }
}
