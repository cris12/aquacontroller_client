package com.aerosoft.aquacontroller.Controller.DataListener;

import com.aerosoft.aquacontroller.Model.DataModel.DeviceSecondsTimer;

/**
 * Created by Vadim on 06.12.2016.
 * Interface for listener changed DataSecondsTimer in List Adapter
 */

public interface IDataSecondsTimerListener {
    void onEvent(DeviceSecondsTimer e);
}
