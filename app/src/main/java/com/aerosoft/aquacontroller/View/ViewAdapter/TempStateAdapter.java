package com.aerosoft.aquacontroller.View.ViewAdapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.aerosoft.aquacontroller.Controller.Chart.ChartController;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTempSensors;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTempState;
import com.aerosoft.aquacontroller.Controller.DataListener.IDataTempStateListener;
import com.aerosoft.aquacontroller.Model.DataModel.ManagerStatsSensor;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.Controller.Chart.HelperLineChartView;
import com.github.mikephil.charting.charts.LineChart;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Vadim on 05.12.2016.
 */

public class TempStateAdapter extends RecyclerView.Adapter<TempStateAdapter.TimerViewHolder> {

    private final DeviceTempSensors deviceTempSensors;
    private DeviceInfo deviceInfo;
    private final IDataTempStateListener eventListner;
    private DeviceTempState tempState;
    private Context context;
    private final int[] imagesID = {R.drawable.ic_numeric_1_box, R.drawable.ic_numeric_2_box, R.drawable.ic_numeric_3_box, R.drawable.ic_numeric_4_box, R.drawable.ic_numeric_5_box, R.drawable.ic_numeric_6_box, R.drawable.ic_numeric_7_box, R.drawable.ic_numeric_8_box};
    private boolean isSpinnerTouched;

    public TempStateAdapter(Context context, DeviceTempState tempState, DeviceInfo deviceInfo, DeviceTempSensors deviceTempSensors, IDataTempStateListener eventListner) {
        this.tempState = tempState;
        this.context = context;
        this.deviceInfo = deviceInfo;
        this.deviceTempSensors = deviceTempSensors;
        this.eventListner = eventListner;

    }

    @Override
    public TimerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        return new TimerViewHolder(inflater.inflate(R.layout.temp_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final TimerViewHolder holder, final int position) {
        if (deviceTempSensors.getSensors().get(position) == 0) {
            holder.spChannel.setEnabled(false);
            holder.timerStart.setEnabled(false);
            holder.switchTimer.setEnabled(false);
            holder.timerStart.setEnabled(false);
            holder.timerEnd.setEnabled(false);
            holder.chanalImage.setVisibility(View.GONE);
            holder.chartTempListView.setVisibility(View.GONE);
            holder.textTemp.setVisibility(View.GONE);
            return;
        }

        SpinnerAdapter customAdapter = new SpinnerAdapter(context, imagesID, deviceInfo.GetMaxCanal());
        holder.spChannel.setAdapter(customAdapter);
        holder.spChannel.setSelection(tempState.getTemp_timer_chanal().get(position));
        holder.spChannel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                isSpinnerTouched = true;
                return false;
            }
        });
        holder.spChannel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                if (tempState.getTemp_timer_chanal().get(position) != pos) {
                    tempState.getTemp_timer_chanal().set(position, pos);
                    eventListner.onEvent(tempState);
                    if (isSpinnerTouched) {
                        isSpinnerTouched = false;

                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (holder.statsTempSensor != null && deviceInfo != null) {
            ChartController chartController = new ChartController(context, deviceInfo, holder.statsTempSensor);
            HelperLineChartView.Builder builder = new HelperLineChartView.Builder(new LineChart[]{holder.chartTemp}, HelperLineChartView.ChartView.CHART_VIEW_BUTTON, context);
            try {
                builder.SetChatLineDataSet(new List[]{chartController.GetEntriesForTemp(position)}, new int[]{R.drawable.fade_list_green, R.drawable.fade_list_yellow}, 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            builder.SetChatView();
            builder.SetXAxis();
            builder.SetYAxis();
            builder.Build();
        }
        if (holder.statsTempSensor.getSensorState().size() > 0) {
            int lastPosition = holder.statsTempSensor.getSensorState().get(position).size();
            float temp1 = (float) (holder.statsTempSensor.getSensorState().get(position).get(lastPosition - 1) * 25 + this.deviceInfo.getMin_temp()) / 100;
            holder.textTemp.setText(temp1 + "C°");
        }
        if (holder.controlsView.getVisibility() == View.GONE) {
            holder.chanalImage.setImageResource(imagesID[tempState.getTemp_timer_chanal().get(position)]);
        } else {
            holder.chanalImage.setImageResource(R.drawable.ic_up_arrow_angle);
        }

        holder.timerStart.setText(String.format(Locale.ENGLISH, "%05.02f", (float) (tempState.getTemp_timer_min_start().get(position) * 25 + this.deviceInfo.getMin_temp()) / 100));
        holder.timerEnd.setText(String.format(Locale.ENGLISH, "%05.02f", (float) (tempState.getTemp_timer_max_end().get(position) * 25 + this.deviceInfo.getMin_temp()) / 100));
        holder.timerStartChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog d = new Dialog(context);
                d.requestWindowFeature(Window.FEATURE_NO_TITLE);
                d.setContentView(R.layout.number_dialog);
                ImageButton minus = d.findViewById(R.id.btnMinus);
                ImageButton plus = d.findViewById(R.id.btnPlus);
                final EditText tempText = d.findViewById(R.id.editTemp);
                tempText.setEnabled(false);
                final Button b1 = d.findViewById(R.id.button1);
                b1.setText(context.getString(R.string.button_cancel));
                TextView title = d.findViewById(R.id.dialog_title);
                title.setText(context.getString(R.string.dialog_title_temp));
                tempText.setText(String.format(Locale.ENGLISH, "%05.02f",
                        (float) (tempState.getTemp_timer_min_start().get(position) * 25 + deviceInfo.getMin_temp()) / 100));
                minus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        b1.setText(context.getString(R.string.button_ok));
                        Integer val = tempState.getTemp_timer_min_start().get(position);
                        if (val > 0) {
                            val--;
                            tempState.getTemp_timer_min_start().set(position, val);
                            tempText.setText(String.format(Locale.ENGLISH, "%05.02f",
                                    (float) (tempState.getTemp_timer_min_start().get(position) * 25 + deviceInfo.getMin_temp()) / 100));
                        }
                    }
                });

                plus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        b1.setText(context.getString(R.string.button_ok));
                        Integer val = tempState.getTemp_timer_min_start().get(position);
                        if (val < (deviceInfo.getMax_temp() - deviceInfo.getMin_temp()) / 25) {
                            val++;
                            tempState.getTemp_timer_min_start().set(position, val);
                            tempText.setText(String.format(Locale.ENGLISH, "%05.02f",
                                    (float) (tempState.getTemp_timer_min_start().get(position) * 25 + deviceInfo.getMin_temp()) / 100));
                        }
                    }
                });

                b1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        holder.timerStart.setText(String.format(Locale.ENGLISH, "%05.02f", (float) (tempState.getTemp_timer_min_start().get(position) * 25 + deviceInfo.getMin_temp()) / 100));
                        eventListner.onEvent(tempState);
                        Log.d(getClass().getName().toString(), tempState.getTemp_timer_min_start().toString());
                        d.dismiss();

                    }
                });
                d.show();
            }
        });
        holder.timerEndChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog d = new Dialog(context);
                d.requestWindowFeature(Window.FEATURE_NO_TITLE);
                d.setContentView(R.layout.number_dialog);
                ImageButton minus = d.findViewById(R.id.btnMinus);
                ImageButton plus = d.findViewById(R.id.btnPlus);
                final EditText tempText = d.findViewById(R.id.editTemp);
                tempText.setEnabled(false);
                final Button b1 = d.findViewById(R.id.button1);
                b1.setText(context.getString(R.string.button_cancel));
                TextView title = d.findViewById(R.id.dialog_title);
                title.setText(context.getString(R.string.dialog_title_temp));
                tempText.setText(String.format(Locale.ENGLISH, "%05.02f",
                        (float) (tempState.getTemp_timer_max_end().get(position) * 25 + deviceInfo.getMin_temp()) / 100));
                minus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        b1.setText(context.getString(R.string.button_ok));
                        Integer val = tempState.getTemp_timer_max_end().get(position);
                        if (val > 0) {
                            val--;
                            tempState.getTemp_timer_max_end().set(position, val);
                            tempText.setText(String.format(Locale.ENGLISH, "%05.02f",
                                    (float) (tempState.getTemp_timer_max_end().get(position) * 25 + deviceInfo.getMin_temp()) / 100));
                        }
                    }
                });

                plus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        b1.setText(context.getString(R.string.button_ok));
                        Integer val = tempState.getTemp_timer_max_end().get(position);
                        if (val < (deviceInfo.getMax_temp() - deviceInfo.getMin_temp()) / 25) {
                            val++;
                            tempState.getTemp_timer_max_end().set(position, val);
                            tempText.setText(String.format(Locale.ENGLISH, "%05.02f",
                                    (float) (tempState.getTemp_timer_max_end().get(position) * 25 + deviceInfo.getMin_temp()) / 100));
                        }
                    }
                });
                b1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.timerEnd.setText(String.format(Locale.ENGLISH, "%05.02f", (float) (tempState.getTemp_timer_max_end().get(position) * 25 + deviceInfo.getMin_temp()) / 100));
                        eventListner.onEvent(tempState);
                        Log.d(getClass().getName().toString(), tempState.getTemp_timer_max_end().toString());
                        d.dismiss();

                    }
                });

                d.show();
            }
        });
        holder.switchTimer.setChecked(tempState.getTemp_timer_state().get(position) == 1);
        holder.switchTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tempState.getTemp_timer_state().get(position) == 0)
                    tempState.getTemp_timer_state().set(position, 1);
                else
                    tempState.getTemp_timer_state().set(position, 0);
                if (tempState.getTemp_timer_state().get(position) == 0) {
                    holder.timerEnable.setText(context.getString(R.string.OFF));
                } else {
                    holder.timerEnable.setText(context.getString(R.string.ON));
                }
                eventListner.onEvent(tempState);
                Log.d(getClass().getName().toString(), tempState.getTemp_timer_state().toString());
            }
        });
        if (tempState.getTemp_timer_state().get(position) == 0) {
            holder.timerEnable.setText(context.getString(R.string.OFF));
        } else {
            holder.timerEnable.setText(context.getString(R.string.ON));
        }
    }

    @Override
    public int getItemCount() {
        return deviceInfo.getMax_temp_sensor();
    }

    class TimerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.controls_view)
        View controlsView;

        //group
        @BindView(R.id.group_timer_daily_chanal)
        ImageView chanalImage;
        @BindView(R.id.group_timer_daily_start)
        TextView timerStart;
        @BindView(R.id.group_timer_daily_end)
        TextView timerEnd;
        @BindView(R.id.group_timer_daily_enable)
        TextView timerEnable;
        //child
        @BindView(R.id.child_timer_start)
        ImageButton timerStartChild;
        @BindView(R.id.child_timer_end)
        ImageButton timerEndChild;
        @BindView(R.id.child_timer_swith)
        SwitchCompat switchTimer;

        @BindView(R.id.spinner_chanal_timer)
        Spinner spChannel;

        @BindView(R.id.chartTempList)
        LineChart chartTemp;
        @BindView(R.id.chartTempListView)
        RelativeLayout chartTempListView;

        @BindView(R.id.textTemp)
        TextView textTemp;

        TimerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        ManagerStatsSensor statsTempSensor = ManagerStatsSensor.getInstance(context);

        @OnClick(R.id.timer_view)
        void onTimerViewClicked() {
            if (controlsView.getVisibility() == View.VISIBLE) {
                notifyDataSetChanged();
                controlsView.setVisibility(View.GONE);
            } else {
                controlsView.setVisibility(View.VISIBLE);
            }
        }

    }
}
