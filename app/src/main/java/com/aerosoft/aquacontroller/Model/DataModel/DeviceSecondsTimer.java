package com.aerosoft.aquacontroller.Model.DataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Vadim on 22.11.2016.
 */

public class DeviceSecondsTimer implements Cloneable, IValidateData {
    @SerializedName("st_h_s")
    private ArrayList<Integer> second_timer_hour_start;
    @SerializedName("st_m_s")
    private ArrayList<Integer> second_timer_min_start;
    @SerializedName("st_d")
    private ArrayList<Integer> second_timer_duration;
    @SerializedName("st_s")
    private ArrayList<Integer> second_timer_state;
    @SerializedName("st_c")
    private ArrayList<Integer> second_timer_canal;

    public ArrayList<Integer> getSecond_timer_hour_start() {
        return second_timer_hour_start;
    }

    public ArrayList<Integer> getSecond_timer_min_start() {
        return second_timer_min_start;
    }

    public ArrayList<Integer> getSecond_timer_duration() {
        return second_timer_duration;
    }

    public ArrayList<Integer> getSecond_timer_state() {
        return second_timer_state;
    }

    public ArrayList<Integer> getSecond_timer_canal() {
        return second_timer_canal;
    }

    public DeviceSecondsTimer clone() throws CloneNotSupportedException {
        DeviceSecondsTimer newDeviceSecondsTimer = (DeviceSecondsTimer) super.clone();
        newDeviceSecondsTimer.second_timer_hour_start = (ArrayList<Integer>) second_timer_hour_start.clone();
        newDeviceSecondsTimer.second_timer_min_start = (ArrayList<Integer>) second_timer_min_start.clone();
        newDeviceSecondsTimer.second_timer_duration = (ArrayList<Integer>) second_timer_duration.clone();
        newDeviceSecondsTimer.second_timer_state = (ArrayList<Integer>) second_timer_state.clone();
        newDeviceSecondsTimer.second_timer_canal = (ArrayList<Integer>) second_timer_canal.clone();
        return newDeviceSecondsTimer;
    }

    @Override
    public boolean IsValidate() {
        if (second_timer_hour_start == null || second_timer_min_start == null ||
                second_timer_duration == null || second_timer_state == null ||
                second_timer_canal == null)
            return false;
        int size = second_timer_hour_start.size();
        return !(size != second_timer_min_start.size() || size != second_timer_duration.size() ||
                size != second_timer_state.size() || size != second_timer_canal.size());
    }
}
