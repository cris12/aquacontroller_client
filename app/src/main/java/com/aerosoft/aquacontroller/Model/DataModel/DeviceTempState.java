package com.aerosoft.aquacontroller.Model.DataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Vadim on 21.11.2016.
 */

public class DeviceTempState implements Cloneable, IValidateData {
    @SerializedName("tt_s")
    private ArrayList<Integer> temp_timer_state;

    @SerializedName("tt_m_s")
    private ArrayList<Integer> temp_timer_min_start;

    @SerializedName("tt_m_e")
    private ArrayList<Integer> temp_timer_max_end;

    @SerializedName("tt_c")
    private ArrayList<Integer> temp_timer_canal;

    public ArrayList<Integer> getTemp_timer_state() {
        return temp_timer_state;
    }

    public ArrayList<Integer> getTemp_timer_min_start() {
        return temp_timer_min_start;
    }

    public ArrayList<Integer> getTemp_timer_max_end() {
        return temp_timer_max_end;
    }

    public ArrayList<Integer> getTemp_timer_chanal() {
        return temp_timer_canal;
    }

    public DeviceTempState clone() throws CloneNotSupportedException {
        DeviceTempState newDeviceTempState = (DeviceTempState) super.clone();
        newDeviceTempState.temp_timer_state = (ArrayList<Integer>) temp_timer_state.clone();
        newDeviceTempState.temp_timer_min_start = (ArrayList<Integer>) temp_timer_min_start.clone();
        newDeviceTempState.temp_timer_max_end = (ArrayList<Integer>) temp_timer_max_end.clone();
        newDeviceTempState.temp_timer_canal = (ArrayList<Integer>) temp_timer_canal.clone();

        return newDeviceTempState;
    }

    @Override
    public boolean IsValidate() {
        if (temp_timer_state == null || temp_timer_min_start == null || temp_timer_max_end == null || temp_timer_canal == null)
            return false;
        int size = temp_timer_state.size();
        return !(size != temp_timer_min_start.size() || size != temp_timer_max_end.size() || size != temp_timer_canal.size());
    }
}
