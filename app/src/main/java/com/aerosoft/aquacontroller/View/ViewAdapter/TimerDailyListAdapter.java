package com.aerosoft.aquacontroller.View.ViewAdapter;

import android.app.TimePickerDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTimer;
import com.aerosoft.aquacontroller.Controller.DataListener.IDataDailyTimerListener;
import com.aerosoft.aquacontroller.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Vadim on 28.11.2016.
 */

public class TimerDailyListAdapter extends RecyclerView.Adapter<TimerDailyListAdapter.TimerViewHolder> {

    private DeviceInfo deviceInfo;
    private final IDataDailyTimerListener eventListner;
    private DeviceTimer timers;
    private Context mContext;
    private final int[] imagesID = {R.drawable.ic_numeric_1_box, R.drawable.ic_numeric_2_box, R.drawable.ic_numeric_3_box, R.drawable.ic_numeric_4_box, R.drawable.ic_numeric_5_box, R.drawable.ic_numeric_6_box, R.drawable.ic_numeric_7_box, R.drawable.ic_numeric_8_box};

    public TimerDailyListAdapter(Context mContext, DeviceTimer timers, DeviceInfo deviceInfo, IDataDailyTimerListener eventListner) {
        this.timers = timers;
        this.mContext = mContext;
        this.deviceInfo = deviceInfo;
        this.eventListner = eventListner;
    }


    @Override
    public TimerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        return new TimerViewHolder(inflater.inflate(R.layout.timer_list_item, parent, false));
    }

    private boolean isSpinnerTouched = false;

    @Override
    public void onBindViewHolder(final TimerViewHolder holder, final int position) {

        SpinnerAdapter customAdapter = new SpinnerAdapter(mContext, imagesID, deviceInfo.GetMaxCanal());
        holder.spChannel.setAdapter(customAdapter);
        holder.spChannel.setSelection(timers.getDaily_timer_canal().get(position));
        holder.spChannel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                isSpinnerTouched = true;
                return false;
            }
        });
        holder.spChannel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                if (timers.getDaily_timer_canal().get(position) != pos) {
                    timers.getDaily_timer_canal().set(position, pos);
                    eventListner.onEvent(timers);
                    Log.d(getClass().getName().toString(), timers.getDaily_timer_canal().toString());
                    if (isSpinnerTouched) {
                        isSpinnerTouched = false;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (holder.controlsView.getVisibility() == View.GONE) {
            holder.chanalImage.setImageResource(imagesID[timers.getDaily_timer_canal().get(position)]);
        } else {
            holder.chanalImage.setImageResource(R.drawable.ic_up_arrow_angle);
        }

        holder.timerStart.setText(String.format("%02d:%02d", timers.getDaily_timer_hour_start().get(position), timers.getDaily_timer_min_start().get(position)));
        holder.timerEnd.setText(String.format("%02d:%02d", timers.getDaily_timer_hour_end().get(position), timers.getDaily_timer_min_end().get(position)));
        holder.timerStartChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(
                        mContext,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                timers.getDaily_timer_hour_start().set(position, hourOfDay);
                                timers.getDaily_timer_min_start().set(position, minute);
                                holder.timerStart.setText(String.format("%02d:%02d", timers.getDaily_timer_hour_start().get(position), timers.getDaily_timer_min_start().get(position)));
                                eventListner.onEvent(timers);
                                Log.d(getClass().getName().toString(), timers.getDaily_timer_hour_start().toString());
                                Log.d(getClass().getName().toString(), timers.getDaily_timer_min_start().toString());

                            }
                        },
                        timers.getDaily_timer_hour_start().get(position),
                        timers.getDaily_timer_min_start().get(position),
                        true).show();
            }
        });
        holder.timerEndChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(mContext,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                timers.getDaily_timer_hour_end().set(position, hourOfDay);
                                timers.getDaily_timer_min_end().set(position, minute);
                                holder.timerEnd.setText(String.format("%02d:%02d", timers.getDaily_timer_hour_end().get(position), timers.getDaily_timer_min_end().get(position)));
                                Log.d(getClass().getName().toString(), timers.getDaily_timer_hour_end().toString());
                                Log.d(getClass().getName().toString(), timers.getDaily_timer_min_end().toString());
                                eventListner.onEvent(timers);

                            }
                        },
                        timers.getDaily_timer_hour_end().get(position),
                        timers.getDaily_timer_min_end().get(position),
                        true).show();
            }
        });
        holder.switchTimer.setChecked(timers.getDaily_timer_state().get(position) == 1);
        holder.switchTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timers.getDaily_timer_state().get(position) == 0)
                    timers.getDaily_timer_state().set(position, 1);
                else
                    timers.getDaily_timer_state().set(position, 0);

                if (timers.getDaily_timer_state().get(position) == 0) {
                    holder.timerEnable.setText(mContext.getString(R.string.OFF));
                } else {
                    holder.timerEnable.setText(mContext.getString(R.string.ON));
                }
                eventListner.onEvent(timers);
                Log.d(getClass().getName().toString(), timers.getDaily_timer_state().toString());
            }
        });
        if (timers.getDaily_timer_state().get(position) == 0) {
            holder.timerEnable.setText(mContext.getString(R.string.OFF));
        } else {
            holder.timerEnable.setText(mContext.getString(R.string.ON));
        }
    }

    @Override
    public int getItemCount() {
        return deviceInfo.getMax_timer();
    }


    class TimerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.controls_view)
        View controlsView;

        //group
        @BindView(R.id.group_timer_daily_chanal)
        ImageView chanalImage;
        @BindView(R.id.group_timer_daily_start)
        TextView timerStart;
        @BindView(R.id.group_timer_daily_end)
        TextView timerEnd;
        @BindView(R.id.group_timer_daily_enable)
        TextView timerEnable;
        //child
        @BindView(R.id.child_timer_start)
        ImageButton timerStartChild;
        @BindView(R.id.child_timer_end)
        ImageButton timerEndChild;
        @BindView(R.id.child_timer_swith)
        SwitchCompat switchTimer;

        @BindView(R.id.spinner_chanal_timer)
        Spinner spChannel;

        TimerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.timer_view)
        void onTimerViewClicked(View timerView) {
            if (controlsView.getVisibility() == View.VISIBLE) {
                controlsView.setVisibility(View.GONE);
                notifyDataSetChanged();
            } else {
                controlsView.setVisibility(View.VISIBLE);
            }
            if (controlsView.getVisibility() != View.GONE) {
                chanalImage.setImageResource(R.drawable.ic_up_arrow_angle);
            }
        }

    }

}
