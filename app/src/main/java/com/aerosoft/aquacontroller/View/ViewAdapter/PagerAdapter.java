package com.aerosoft.aquacontroller.View.ViewAdapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.aerosoft.aquacontroller.View.Fragments.timers.TimersDailyFragment;
import com.aerosoft.aquacontroller.View.Fragments.timers.TimersHoursFragment;
import com.aerosoft.aquacontroller.View.Fragments.timers.TimersSecondFragment;


/**
 * Created by Vadim on 01.12.2016.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new TimersDailyFragment();
            case 1:
                return new TimersHoursFragment();
            case 2:
                return new TimersSecondFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
