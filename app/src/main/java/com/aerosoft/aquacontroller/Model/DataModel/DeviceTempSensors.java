package com.aerosoft.aquacontroller.Model.DataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Vadim on 21.11.2016.
 */

public class DeviceTempSensors implements Cloneable, IValidateData {
    @SerializedName("t_se")
    private ArrayList<Integer> sensors;


    public ArrayList<Integer> getSensors() {
        return sensors;
    }

    public int getAvailableSensorsCount() {
        int result = 0;
        for (Integer sen : getSensors()) {
            if (sen != 0)
                result++;
        }
        return result;
    }

    public DeviceTempSensors clone() throws CloneNotSupportedException {
        DeviceTempSensors newDeviceTempSensors = (DeviceTempSensors) super.clone();
        newDeviceTempSensors.setSensors((ArrayList<Integer>) getSensors().clone());
        return newDeviceTempSensors;
    }

    @Override
    public boolean IsValidate() {
        return getSensors() != null;
    }

    public void setSensors(ArrayList<Integer> sensors) {
        this.sensors = sensors;
    }
}
