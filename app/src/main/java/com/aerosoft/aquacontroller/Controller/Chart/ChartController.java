package com.aerosoft.aquacontroller.Controller.Chart;

import android.content.Context;
import java.util.Calendar;

import com.aerosoft.aquacontroller.Controller.DataHelper.UdpHelper;
import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceCanalState;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceHoursTimer;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceSecondsTimer;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTimer;
import com.aerosoft.aquacontroller.Model.DataModel.ManagerStatsSensor;
import com.aerosoft.aquacontroller.View.Fragments.Preference.ShareManager;
import com.github.mikephil.charting.data.Entry;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by DrJar on 15.08.2017.
 */

public class ChartController {

    private final Context context;
    private List<Entry> entries;
    private List<Float> dataPower;
    private List<Float> dataTemp;
    private ArrayList<Integer> power;
    private ManagerStatsSensor statsTempSensor;
    private DeviceTimer deviceTimer;
    private DeviceSecondsTimer deviceSecondsTimer;
    private DeviceHoursTimer deviceHoursTimer;
    private DeviceInfo deviceInfo;
    private DeviceCanalState deviceCanalState;


    public ChartController(Context context, DeviceInfo deviceInfo, ManagerStatsSensor statsTempSensor) {
        this.context = context;
        this.statsTempSensor = statsTempSensor;
        this.deviceInfo = deviceInfo;
        dataTemp = new ArrayList<>();
    }

    public ChartController(Context context, DeviceCanalState deviceCanalState, DeviceInfo deviceInfo, DeviceTimer deviceTimer,
                           DeviceSecondsTimer deviceSecondsTimer, DeviceHoursTimer deviceHoursTimer) {
        this.deviceInfo = deviceInfo;
        this.context = context;
        this.deviceTimer = deviceTimer;
        this.deviceHoursTimer = deviceHoursTimer;
        this.deviceSecondsTimer = deviceSecondsTimer;
        this.deviceCanalState = deviceCanalState;

        dataPower = new ArrayList<>();
        for (int i = 0; i < 24; i++)
            dataPower.add(0f);
        power = new ArrayList<>();

        GetPowerPreference(new String[]{"pref_power_1", "pref_power_2", "pref_power_3", "pref_power_4", "pref_power_5", "pref_power_6", "pref_power_7", "pref_power_8"});

    }


    /**
     * Return Entries set for main chart in device state fragment
     *
     * @return List<Entry>
     */
    public List<Entry> GetEntriesPowerByHour() {
        entries = new ArrayList<>();
        if (UdpHelper.IS_TEST) {
            for (int i = 0; i < 8; i++) {
                Random rand = new Random();
                int start = rand.nextInt(23);
                int end = rand.nextInt(23);
                int power = rand.nextInt(200);
                HelpPowerByHour(start, end, power);
            }
        } else {
            for (int i = 0; i < deviceInfo.getMax_timer(); i++) {

                if (deviceCanalState.getCanal_timer().get(deviceTimer.getDaily_timer_canal().get(i))
                        == ProtocolHelper.CHANAL_AUTO
                        && deviceTimer.getDaily_timer_state().get(i) == ProtocolHelper.TIMER_ON)
                    HelpPowerByHour(deviceTimer.getDaily_timer_hour_start().get(i),
                            deviceTimer.getDaily_timer_hour_end().get(i), power.get(deviceTimer.getDaily_timer_canal().get(i)));

                if (deviceCanalState.getCanal_timer().get(deviceHoursTimer.getHours_timer_canal().get(i))
                        == ProtocolHelper.CHANAL_AUTO
                        && deviceHoursTimer.getHours_timer_state().get(i) == ProtocolHelper.TIMER_ON)
                    HelpPowerByMin(deviceHoursTimer.getHours_timer_min_start().get(i),
                            deviceHoursTimer.getHours_timer_min_stop().get(i), power.get(deviceHoursTimer.getHours_timer_canal().get(i)));

                if (deviceCanalState.getCanal_timer().get(deviceSecondsTimer.getSecond_timer_canal().get(i))
                        == ProtocolHelper.CHANAL_AUTO
                        && deviceSecondsTimer.getSecond_timer_state().get(i) == ProtocolHelper.TIMER_ON)
                    HelpPowerBySecond(deviceSecondsTimer.getSecond_timer_hour_start().get(i),
                            deviceSecondsTimer.getSecond_timer_duration().get(i), power.get(deviceSecondsTimer.getSecond_timer_canal().get(i)));
            }
        }
        for (int i = 0; i < deviceCanalState.getCanal_timer().size(); i++) {
            if (deviceCanalState.getCanal_timer().get(i) == ProtocolHelper.CHANAL_ON) {
                HelpPowerByHour(0, 23, power.get(i));
            }
        }

        for (int i = 0; i < dataPower.size(); i++)
            entries.add(new Entry(i, dataPower.get(i)));
        return entries;
    }

    /**
     * Use for show power statistics in TextView on device state fragment
     *
     * @return List<hour, day, month>
     */
    public List<Float> GetPowerStatistic() {
        Calendar myCalendar = (Calendar) Calendar.getInstance().clone();
        myCalendar.set(myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), 1);
        int max_date = myCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        List<Float> powerStats = new ArrayList<>();
        Float result = 0f;
        for (int i = 0; i < 24; i++) {
            result += dataPower.get(i);
        }
        powerStats.add(result / 24);
        powerStats.add(result / 1000);
        powerStats.add(result * max_date / 1000);

        return powerStats;
    }

    public List<Entry> GetEntriesForTemp(int sensor) throws Exception {
        return GetEntriesForTemp(sensor, 5);
    }

    public List<Entry> GetEntriesForTemp(int sensor, int count) throws Exception {
        if (sensor > this.deviceInfo.getMax_temp_sensor())
            throw new Exception("Index sensor out of range!");
        dataTemp = new ArrayList<>();
        entries = new ArrayList<>();
        if (UdpHelper.IS_TEST) {
            for (int i = 0; i < count; i++) {
                Random rand = new Random();
                float temp1 = (float) (rand.nextInt(com.aerosoft.aquacontroller.Controller.DataHelper.Constants.MAX_TEMP_INDEX) * 25 + this.deviceInfo.getMin_temp()) / 100;
                dataTemp.add(temp1);
            }
        } else {
            if (statsTempSensor.getSensorState().get(sensor).size() < count) {
                count = statsTempSensor.getSensorState().get(sensor).size();
            }
            if (statsTempSensor.getSensorTime().get(sensor).size() < count) {
                count = statsTempSensor.getSensorTime().get(sensor).size();
            }
            for (int i = 0; i < statsTempSensor.getSensorState().get(sensor).size(); i++) {
                if (i > count - 1) break;
                int lastIndex = statsTempSensor.getSensorState().get(sensor).size() - 1;
                lastIndex = lastIndex - i;
                if (lastIndex < 0) break;
                float temp1 = (float) (statsTempSensor.getSensorState().get(sensor).get(lastIndex) * 25 + this.deviceInfo.getMin_temp()) / 100;
                dataTemp.add(temp1);
            }
        }
        Collections.reverse(dataTemp);
        for (int i = 0; i < dataTemp.size(); i++)
            entries.add(new Entry(i, dataTemp.get(i)));
        return entries;
    }


    private void HelpPowerByMin(int start, int end, int power) {
        float powerInMin = power / 60;
        for (int i = 0; i < 24; i++) {
            dataPower.set(i, dataPower.get(i) + powerInMin * Math.abs(end - start));
        }
    }

    private void HelpPowerBySecond(int start, int second, int power) {
        float powerInMin = (float) power / 3600;
        dataPower.set(start, dataPower.get(start) + powerInMin * second);
    }

    private void HelpPowerByHour(int start, int end, int power) {

        if (start < end) {
            for (int i = start; i < end + 1; i++)
                dataPower.set(i, dataPower.get(i) + power);
        } else if (start > end) {
            for (int i = start; i < 24; i++)
                dataPower.set(i, dataPower.get(i) + power);
            for (int i = 0; i < end; i++)
                dataPower.set(i, dataPower.get(i) + power);
        } else {
            dataPower.set(start, dataPower.get(start) + power);
        }
    }

    private void GetPowerPreference(String[] key) {
        for (String k : key) {
            power.add(ShareManager.getInstance(context).GetPowerPreference(k));
        }

    }
}
