package com.aerosoft.aquacontroller.View.ViewAdapter;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceSecondsTimer;
import com.aerosoft.aquacontroller.Controller.DataListener.IDataSecondsTimerListener;
import com.aerosoft.aquacontroller.R;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Vadim on 02.12.2016.
 */

public class TimerSecondListAdapter extends RecyclerView.Adapter<TimerSecondListAdapter.TimerViewHolder> {

    private DeviceInfo deviceInfo;
    private final IDataSecondsTimerListener eventListner;
    private DeviceSecondsTimer timers;
    private Context mContext;
    private final int[] imagesID = {R.drawable.ic_numeric_1_box, R.drawable.ic_numeric_2_box, R.drawable.ic_numeric_3_box, R.drawable.ic_numeric_4_box, R.drawable.ic_numeric_5_box, R.drawable.ic_numeric_6_box, R.drawable.ic_numeric_7_box, R.drawable.ic_numeric_8_box};
    private boolean isSpinnerTouched;

    public TimerSecondListAdapter(Context mContext, DeviceSecondsTimer timers, DeviceInfo deviceInfo, IDataSecondsTimerListener eventListner) {
        this.timers = timers;
        this.mContext = mContext;
        this.deviceInfo = deviceInfo;
        this.eventListner = eventListner;
    }

    @Override
    public TimerSecondListAdapter.TimerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        return new TimerSecondListAdapter.TimerViewHolder(inflater.inflate(R.layout.timer_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final TimerSecondListAdapter.TimerViewHolder holder, final int position) {
        SpinnerAdapter customAdapter = new SpinnerAdapter(mContext, imagesID, deviceInfo.GetMaxCanal());
        holder.spChannel.setAdapter(customAdapter);
        holder.spChannel.setSelection(timers.getSecond_timer_canal().get(position));
        holder.spChannel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                isSpinnerTouched = true;
                return false;
            }
        });
        holder.spChannel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                if (timers.getSecond_timer_canal().get(position) != pos) {
                    timers.getSecond_timer_canal().set(position, pos);
                    Log.d(getClass().getName().toString(), timers.getSecond_timer_canal().toString());
                    eventListner.onEvent(timers);
                    if (isSpinnerTouched) {
                        isSpinnerTouched = false;

                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (holder.controlsView.getVisibility() == View.GONE) {
            holder.chanalImage.setImageResource(imagesID[timers.getSecond_timer_canal().get(position)]);
        } else {
            holder.chanalImage.setImageResource(R.drawable.ic_up_arrow_angle);
        }

        holder.timerStart.setText(String.format("%02d:%02d", timers.getSecond_timer_hour_start().get(position), timers.getSecond_timer_min_start().get(position)));
        holder.timerEnd.setText(String.format("%03d", timers.getSecond_timer_duration().get(position)) + " " + mContext.getString(R.string.sec_item_list));
        holder.timerStartChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(
                        mContext,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                timers.getSecond_timer_hour_start().set(position, hourOfDay);
                                timers.getSecond_timer_min_start().set(position, minute);
                                holder.timerStart.setText(String.format("%02d:%02d", timers.getSecond_timer_hour_start().get(position), timers.getSecond_timer_min_start().get(position)));
                                Log.d(getClass().getName().toString(), timers.getSecond_timer_min_start().toString());
                                eventListner.onEvent(timers);

                            }
                        },
                        0,
                        timers.getSecond_timer_min_start().get(position),
                        true).show();
            }
        });
        holder.timerEndChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog d = new Dialog(mContext);
                d.requestWindowFeature(Window.FEATURE_NO_TITLE);
                d.setContentView(R.layout.number_dialog);
                ImageButton minus = d.findViewById(R.id.btnMinus);
                ImageButton plus = d.findViewById(R.id.btnPlus);
                final EditText tempText = d.findViewById(R.id.editTemp);
                tempText.setEnabled(false);
                final Button b1 = d.findViewById(R.id.button1);
                b1.setText(mContext.getString(R.string.button_cancel));
                ImageView imageTitle = d.findViewById(R.id.imageView12);
                imageTitle.setImageResource(R.mipmap.time_icon_6);
                TextView title = d.findViewById(R.id.dialog_title);
                title.setText(mContext.getString(R.string.dialog_title_seconds));
                tempText.setText(timers.getSecond_timer_duration().get(position).toString());

                minus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        b1.setText(mContext.getString(R.string.button_ok));
                        Integer val = timers.getSecond_timer_duration().get(position);
                        if (val > 0) {
                            val--;
                            timers.getSecond_timer_duration().set(position, val);
                            tempText.setText(timers.getSecond_timer_duration().get(position).toString());
                        }
                    }
                });
                plus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        b1.setText(mContext.getString(R.string.button_ok));
                        Integer val = timers.getSecond_timer_duration().get(position);
                        if (val < 255) {
                            if (255 - val >= 10)
                                val += 10;
                            else
                                val += 255 - val;
                            timers.getSecond_timer_duration().set(position, val);
                            tempText.setText(timers.getSecond_timer_duration().get(position).toString());
                        }
                    }
                });
                b1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.timerEnd.setText(String.format("%03d", timers.getSecond_timer_duration().get(position)) + " " + mContext.getString(R.string.sec_item_list));
                        eventListner.onEvent(timers);
                        Log.d(getClass().getName().toString(), timers.getSecond_timer_duration().toString());
                        d.dismiss();

                    }
                });

                d.show();
            }
        });
        holder.switchTimer.setChecked(timers.getSecond_timer_state().get(position) == 1);
        holder.switchTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timers.getSecond_timer_state().get(position) == 0)
                    timers.getSecond_timer_state().set(position, 1);
                else
                    timers.getSecond_timer_state().set(position, 0);
                if (timers.getSecond_timer_state().get(position) == 0) {
                    holder.timerEnable.setText(mContext.getString(R.string.OFF));
                } else {
                    holder.timerEnable.setText(mContext.getString(R.string.ON));
                }
                Log.d(getClass().getName().toString(), timers.getSecond_timer_state().toString());
                eventListner.onEvent(timers);

            }
        });
        if (timers.getSecond_timer_state().get(position) == 0) {
            holder.timerEnable.setText(mContext.getString(R.string.OFF));
        } else {
            holder.timerEnable.setText(mContext.getString(R.string.ON));
        }
    }

    @Override
    public int getItemCount() {
        return deviceInfo.getMax_timer();
    }

    class TimerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.controls_view)
        View controlsView;

        //group
        @BindView(R.id.group_timer_daily_chanal)
        ImageView chanalImage;
        @BindView(R.id.group_timer_daily_start)
        TextView timerStart;
        @BindView(R.id.group_timer_daily_end)
        TextView timerEnd;
        @BindView(R.id.group_timer_daily_enable)
        TextView timerEnable;
        //child
        @BindView(R.id.child_timer_start)
        ImageButton timerStartChild;
        @BindView(R.id.child_timer_end)
        ImageButton timerEndChild;
        @BindView(R.id.child_timer_swith)
        SwitchCompat switchTimer;

        @BindView(R.id.spinner_chanal_timer)
        Spinner spChannel;

        TimerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.timer_view)
        void onTimerViewClicked(View timerView) {
            if (controlsView.getVisibility() == View.VISIBLE) {
                notifyDataSetChanged();
                controlsView.setVisibility(View.GONE);
            } else {
                controlsView.setVisibility(View.VISIBLE);
            }
            if (controlsView.getVisibility() != View.GONE) {
                chanalImage.setImageResource(R.drawable.ic_up_arrow_angle);
            }
        }

    }
}
