package com.aerosoft.aquacontroller.Controller.DataHelper;

import android.support.annotation.Nullable;

import com.aerosoft.aquacontroller.Model.DataModel.DeviceCanalState;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceHoursTimer;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceSecondsTimer;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTempSensors;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTempState;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTimer;
import com.aerosoft.aquacontroller.Model.TaskResponse.TaskResponse;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_DEVICE_CANAL_SETTINGS;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_DEVICE_DAILY_TIMER_SATE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.UdpHelper.REQUEST_DEVICE_FIELD_NAME;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_DEVICE_HOURS_TIMER_SATE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_DEVICE_INFO;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_DEVICE_SECOND_TIMER_SATE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_DEVICE_TEMP_SENSOR;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_DEVICE_TEMP_STATE;

/**
 * Created by Doc on 14.11.2016.
 */

public class GsonHelper {




    /**
     * Convert JSON to Task Object
     * @param json
     * @param <T>
     * @return
     */
    @Nullable
    public static  <T extends Cloneable> ResultTaskResponse<T> JSONtoObject(String json) {
        String TypeMessage = null;
        JsonObject jsonObject;
        Type collectionType = null;
        try {
            jsonObject = (new JsonParser()).parse(json).getAsJsonObject();
        } catch (Exception e) {
            Crashlytics.log(e.getMessage());
            return null;
        }
        if (jsonObject == null) return null;
        String type = jsonObject.get(REQUEST_DEVICE_FIELD_NAME).getAsString();
        if (type.equals(REQUEST_DEVICE_INFO)) {
            TypeMessage = REQUEST_DEVICE_INFO;
            collectionType = new TypeToken<TaskResponse<DeviceInfo>>() {
            }.getType();
        } else if (type.equals(REQUEST_DEVICE_CANAL_SETTINGS)) {
            TypeMessage = REQUEST_DEVICE_CANAL_SETTINGS;
            collectionType = new TypeToken<TaskResponse<DeviceCanalState>>() {
            }.getType();
        } else if (type.equals(REQUEST_DEVICE_DAILY_TIMER_SATE)) {
            TypeMessage = REQUEST_DEVICE_DAILY_TIMER_SATE;
            collectionType = new TypeToken<TaskResponse<DeviceTimer>>() {
            }.getType();
        } else if (type.equals(REQUEST_DEVICE_HOURS_TIMER_SATE)) {
            TypeMessage = REQUEST_DEVICE_HOURS_TIMER_SATE;
            collectionType = new TypeToken<TaskResponse<DeviceHoursTimer>>() {
            }.getType();
        } else if (type.equals(REQUEST_DEVICE_SECOND_TIMER_SATE)) {
            TypeMessage = REQUEST_DEVICE_SECOND_TIMER_SATE;
            collectionType = new TypeToken<TaskResponse<DeviceSecondsTimer>>() {
            }.getType();
        } else if (type.equals(REQUEST_DEVICE_TEMP_STATE)) {
            TypeMessage = REQUEST_DEVICE_TEMP_STATE;
            collectionType = new TypeToken<TaskResponse<DeviceTempState>>() {
            }.getType();
        } else if (type.equals(REQUEST_DEVICE_TEMP_SENSOR)) {
            TypeMessage = REQUEST_DEVICE_TEMP_SENSOR;
            collectionType = new TypeToken<TaskResponse<DeviceTempSensors>>() {
            }.getType();
        }
        try {
            TaskResponse<T> result = new Gson().fromJson(json, collectionType);
            return new ResultTaskResponse<T>(result, TypeMessage);
        } catch (Exception e) {
            Crashlytics.log(e.getMessage());
            return null;
        }

    }
    public static class ResultTaskResponse<T extends Cloneable> {

        private final TaskResponse<T> result;
        private final String typeMessage;

        public ResultTaskResponse(TaskResponse<T> result, String typeMessage){
            this.result = result;
            this.typeMessage = typeMessage;
        }

        public TaskResponse<T> GetResult(){
            return result;
        }

        public String GetTypeMessage(){
            return typeMessage;
        }
    }
}