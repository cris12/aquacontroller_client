package com.aerosoft.aquacontroller.Controller.DataHelper;

import android.app.Application;
import android.content.Context;

import com.aerosoft.aquacontroller.R;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Doc on 14.11.2016.
 */

public class ProtocolHelper {

    public static final int CHANAL_INDEFINED = 0;
    public static final int CHANAL_OFF = 1;
    public static final int CHANAL_ON = 2;
    public static final int CHANAL_AUTO = 3;

    public static final int CHANAL_TIMER_OFF = 1;
    public static final int CHANAL_TIMER_ON = 2;
    public static final int CHANAL_TIMER_MIN = 3;
    public static final int CHANAL_TIMER_OTHER = 4;
    public static final int CHANAL_TIMER_SEC = 5;
    public static final int CHANAL_TIMER_TEMP = 6;
    public static final int CHANAL_TIMER_PWM = 7;
    public static final int CHANAL_TIMER_PH = 8;

    public static final int TIMER_OFF = 0;
    public static final int TIMER_ON = 1;


    public static List<String> DEVICE_TYPE = Arrays.asList("AQ_CH04L", "AQ_CH08W", "AQ_CH08WH");

    public static String GET = "get";
    public static String POST = "post";
    public static String INFO = "info";

    public enum PROTOCOL {
        UNDEFINED,
        GET,
        POST,
        INFO
    }

    public static Map<PROTOCOL, String> ProtocolList = new HashMap<PROTOCOL, String>() {{
        put(PROTOCOL.GET, GET);
        put(PROTOCOL.POST, POST);
        put(PROTOCOL.INFO, INFO);
    }};

    public static String REQUEST_DEVICE_INFO = "dev";
    public static String REQUEST_DEVICE_CANAL_SETTINGS = "c_s";
    public static String REQUEST_DEVICE_TEMP_SENSOR = "t_sen";
    public static String REQUEST_DEVICE_TEMP_STATE = "te_s";
    public static String REQUEST_DEVICE_DAILY_TIMER_SATE = "td_s";
    public static String REQUEST_DEVICE_HOURS_TIMER_SATE = "th_s";
    public static String REQUEST_DEVICE_SECOND_TIMER_SATE = "ts_s";
    public static String REQUEST_DEVICE_LOG_INFO = "device_log";
    public static String REQUEST_DEVICE_NTP_UPDATE = "time_NTP";

    public enum REQUEST_TYPE {
        UNDEFINED,
        REQUEST_DEVICE_INFO,
        REQUEST_DEVICE_TEMP_SENSOR,
        REQUEST_DEVICE_CANAL_STATE,
        REQUEST_DEVICE_TEMP_STATE,
        REQUEST_DEVICE_DAILY_TIMER_SATE,
        REQUEST_DEVICE_HOURS_TIMER_SATE,
        REQUEST_DEVICE_SECOND_TIMER_SATE,
        REQUEST_DEVICE_NTP_UPDATE,
        REQUEST_DEVICE_LOG_INFO
    }

    public static Map<REQUEST_TYPE, String> CommandList = new HashMap<REQUEST_TYPE, String>() {{
        put(REQUEST_TYPE.REQUEST_DEVICE_INFO, REQUEST_DEVICE_INFO);
        put(REQUEST_TYPE.REQUEST_DEVICE_CANAL_STATE, REQUEST_DEVICE_CANAL_SETTINGS);
        put(REQUEST_TYPE.REQUEST_DEVICE_TEMP_SENSOR, REQUEST_DEVICE_TEMP_SENSOR);
        put(REQUEST_TYPE.REQUEST_DEVICE_TEMP_STATE, REQUEST_DEVICE_TEMP_STATE);
        put(REQUEST_TYPE.REQUEST_DEVICE_DAILY_TIMER_SATE, REQUEST_DEVICE_DAILY_TIMER_SATE);
        put(REQUEST_TYPE.REQUEST_DEVICE_HOURS_TIMER_SATE, REQUEST_DEVICE_HOURS_TIMER_SATE);
        put(REQUEST_TYPE.REQUEST_DEVICE_SECOND_TIMER_SATE, REQUEST_DEVICE_SECOND_TIMER_SATE);
        put(REQUEST_TYPE.REQUEST_DEVICE_NTP_UPDATE, REQUEST_DEVICE_NTP_UPDATE);
        put(REQUEST_TYPE.REQUEST_DEVICE_LOG_INFO, REQUEST_DEVICE_LOG_INFO);
    }};

    public enum TaskResult {
        RESULT_INIT_LOGO,
        RESULT_SETTINGS,
        RESULT_CONNECTION,
        RESULT_DEVICE_INFO,
        RESULT_DEVICE_CHANAL_STATE,
        RESULT_DEVICE_TIME_DAILY,
        RESULT_DEVICE_TIME_HOUR,
        RESULT_DEVICE_TIME_SECOND,
        RESULT_DEVICE_TEMP,
        RESULT_DEVICE_TEMP_SENSOR,

        RESULT_SUCCESS,
        RESULT_ERROR_SOCKET_EXCEPTION,
        RESULT_ERROR_UNKNOWN_HOST_EXCEPTION,
        RESULT_ERROR_SOCKET_TIMEOUT_EXCEPTION,
        RESULT_ERROR_IO_EXCEPTION,
        RESULT_ERROR_NOT_RESPONSE,
        RESULT_DATA_CORRUPTED,
        RESULT_DATA_DEVICE_BUSY
    }

    private static Map<TaskResult, String> MessageToastList;

    public static String GetMessageToast(final Context context, TaskResult result) {
        if (MessageToastList == null) {

            MessageToastList = new HashMap<TaskResult, String>() {{
                put(TaskResult.RESULT_SUCCESS, context.getString(R.string.SUCCESS));
                put(TaskResult.RESULT_ERROR_SOCKET_EXCEPTION, context.getString(R.string.SOCKET_EXCEPTION));
                put(TaskResult.RESULT_ERROR_UNKNOWN_HOST_EXCEPTION, context.getString(R.string.UNKNOWN_HOST_EXCEPTION));
                put(TaskResult.RESULT_ERROR_SOCKET_TIMEOUT_EXCEPTION, context.getString(R.string.SOCKET_TIMEOUT_EXCEPTION));
                put(TaskResult.RESULT_ERROR_IO_EXCEPTION, context.getString(R.string.ERROR_IO_EXCEPTION));
                put(TaskResult.RESULT_ERROR_NOT_RESPONSE, context.getString(R.string.NOT_RESPONSE));
                put(TaskResult.RESULT_DATA_CORRUPTED, context.getString(R.string.DATA_CORRUPTED));
                put(TaskResult.RESULT_DATA_DEVICE_BUSY, context.getString(R.string.RESULT_DATA_DEVICE_BUSY));
            }};
        }
        if (MessageToastList.containsKey(result)) {
            return MessageToastList.get(result);
        } else {
            return result.toString();
        }
    }

}
