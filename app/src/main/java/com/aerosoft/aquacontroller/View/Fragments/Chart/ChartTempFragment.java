package com.aerosoft.aquacontroller.View.Fragments.Chart;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;

import com.aerosoft.aquacontroller.Controller.Chart.ChartController;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTempSensors;
import com.aerosoft.aquacontroller.Model.DataModel.ManagerStatsSensor;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.View.ViewAdapter.ChartTempAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChartTempFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChartTempFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChartTempFragment extends DialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    private OnFragmentInteractionListener mListener;
    private ListView lv;
    private DeviceTempSensors deviceTempSensors;
    private DeviceInfo deviceInfo;
    private int maxCountElement = 1;
    public ChartTempFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ChartTempFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChartTempFragment newInstance() {
        ChartTempFragment fragment = new ChartTempFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chart_temp, container, false);
        lv = view.findViewById(R.id.listStateTempChart);
        ((SeekBar)view.findViewById(R.id.seekBarChart)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                maxCountElement  = progress + 1;
                UpdateChartList();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        UpdateChartList();
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
    @Override
    public void onResume() {
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        params.height =  LinearLayout.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        super.onResume();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceInfo event) {
        this.deviceInfo = event;
        UpdateChartList();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceTempSensors event) {
        this.deviceTempSensors = event;
        UpdateChartList();
    }

    private void UpdateChartList() {
        if (deviceInfo != null && deviceTempSensors != null) {
            ChartController chartController = new ChartController(getActivity(), deviceInfo, ManagerStatsSensor.getInstance(getContext()));
            List data = new ArrayList();
            List<Integer> indexTempSensor = new ArrayList<>();
            for (int i = 0; i < deviceTempSensors.getSensors().size(); i++) {
                try {

                    if (deviceTempSensors.getSensors().get(i) > 0) {
                        data.add(chartController.GetEntriesForTemp(i, maxCountElement * 5));
                        indexTempSensor.add(i);
                   }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            ChartTempAdapter adapter = new ChartTempAdapter(getContext(), data, indexTempSensor);
            lv.setAdapter(adapter);
        }
    }
}
