package com.aerosoft.aquacontroller.View.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aerosoft.aquacontroller.Controller.Chart.ChartController;
import com.aerosoft.aquacontroller.Controller.Chart.HelperLineChartView;
import com.aerosoft.aquacontroller.Controller.DataHelper.Constants;
import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.aerosoft.aquacontroller.Controller.DataListener.IRequestListener;
import com.aerosoft.aquacontroller.Controller.DataProvider.RequestManager;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceCanalState;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceHoursTimer;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceSecondsTimer;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTempSensors;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTempState;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTimer;
import com.aerosoft.aquacontroller.Model.DataModel.ManagerStatsSensor;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.View.DeviceActivity;
import com.aerosoft.aquacontroller.View.Fragments.Chart.ChartTempFragment;
import com.aerosoft.aquacontroller.View.Fragments.Preference.ShareManager;
import com.aerosoft.aquacontroller.View.ViewHelper.ToastHelper;
import com.github.mikephil.charting.charts.LineChart;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import java.util.Locale;

import static android.view.View.INVISIBLE;
import static com.aerosoft.aquacontroller.Controller.DataHelper.Constants.STATE_CANAL_TYPE.CANAL_ON;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.CHANAL_AUTO;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.CHANAL_OFF;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.CHANAL_ON;
import static java.lang.String.*;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link StateFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link StateFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StateFragment extends Fragment implements IRequestListener, SharedPreferences.OnSharedPreferenceChangeListener {

    TextView StateDeviceModel, StateDeviceMaxTimers, StateDeviceMaxTempSensors, StateDeviceMaxTemp, StateDeviceMinTemp;
    private FloatingActionButton fab;
    private OnFragmentInteractionListener mListener;
    private TextView StateDeviceTempSensor1;
    private TextView StateDeviceTempSensor2;
    private TextView StateDeviceFilters;
    private TextView StateDeviceLights;
    private TextView StateDeviceHeaters;
    private TextView StateDeviceCO2;
    private TextView StateDeviceOther;
    private TextView TextPowerHour;
    private TextView TextPowerDay;
    private TextView TextPowerMonth;

    private SharedPreferences sharedPreferences;
    private DeviceCanalState _deviceCanalState;
    private LineChart chartPower;
    private LineChart chartTemp1;
    private LineChart chartTemp2;
    private ChartController chartController;

    private DeviceInfo deviceInfo;
    private DeviceTimer deviceTimer;
    private DeviceHoursTimer deviceHoursTimer;
    private DeviceSecondsTimer deviceSecondsTimer;
    private DeviceTempState deviceTempState;
    private ManagerStatsSensor statsTempSensor;
    private RelativeLayout layoutTemp1;
    private RelativeLayout layoutTemp2;
    private ImageView imageWater;
    private ImageView imageLight;
    private ImageView imageHeater;
    private ImageView imageCO2;
    private ImageView imageFan;
    private ShareManager shareManager;
    int attemptCount = Constants.ATTEMPT_COUNT;
    private DeviceCanalState _changeDeviceCanalState;
    private View view;
    private View layout_light;
    private View layout_water;
    private View layout_heater;
    private View layout_co2;
    private View layout_fan;

    public StateFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment StateFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StateFragment newInstance() {
        StateFragment fragment = new StateFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        shareManager = ShareManager.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_state, container, false);
        fab = getActivity().findViewById(R.id.fab);
        StateDeviceModel = view.findViewById(R.id.stateDeviceModel);
        StateDeviceMaxTimers = view.findViewById(R.id.stateDeviceMaxTimers);
        StateDeviceMaxTempSensors = view.findViewById(R.id.stateDeviceMaxTempSensors);
        StateDeviceMaxTemp = view.findViewById(R.id.stateDeviceMaxTemp);
        StateDeviceMinTemp = view.findViewById(R.id.stateDeviceMinTemp);
        StateDeviceTempSensor1 = view.findViewById(R.id.stateDeviceSensor1);
        StateDeviceTempSensor2 = view.findViewById(R.id.stateDeviceSensor2);

        StateDeviceFilters = view.findViewById(R.id.textViewFilter);
        StateDeviceLights = view.findViewById(R.id.textViewLight);
        StateDeviceHeaters = view.findViewById(R.id.textViewHeater);
        StateDeviceCO2 = view.findViewById(R.id.textViewCO2);
        StateDeviceOther = view.findViewById(R.id.textViewOther);

        TextPowerHour = view.findViewById(R.id.textPowerHour);
        TextPowerDay = view.findViewById(R.id.textPowerDay);
        TextPowerMonth = view.findViewById(R.id.textPowerMonth);
        chartPower = view.findViewById(R.id.chart);
        chartTemp1 = view.findViewById(R.id.chartTemp1);
        chartTemp2 = view.findViewById(R.id.chartTemp2);


        imageWater = view.findViewById(R.id.imageWater);
        imageLight = view.findViewById(R.id.imageLight);
        imageHeater = view.findViewById(R.id.imageHeater);
        imageCO2 = view.findViewById(R.id.imageCO2);
        imageFan = view.findViewById(R.id.imageFan);

        layoutTemp1 = view.findViewById(R.id.LayoutTemp1);
        layoutTemp1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenTempFragment();
            }
        });
        layoutTemp2 = view.findViewById(R.id.LayoutTemp2);
        layoutTemp2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenTempFragment();
            }
        });
        layout_water = view.findViewById(R.id.layout_water);
        layout_water.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, getString(R.string.CheckPreferenceWater), Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });
        layout_light = view.findViewById(R.id.layout_light);
        layout_light.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangeCanalState("multi_select_list_preference_light");
            }
        });
        layout_heater = view.findViewById(R.id.layout_heater);
        layout_heater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangeCanalState("multi_select_list_preference_heater");
            }
        });
        layout_co2 = view.findViewById(R.id.layout_co2);
        layout_co2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangeCanalState("multi_select_list_preference_co2");
            }
        });
        layout_fan = view.findViewById(R.id.layout_fan);
        layout_fan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangeCanalState("multi_select_list_preference_fan");
            }
        });
        statsTempSensor = ManagerStatsSensor.getInstance(getActivity());
        return view;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        fab.setVisibility(INVISIBLE);
        mListener = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        fab.setVisibility(INVISIBLE);
        EventBus.getDefault().register(this);

    }

    @Override
    public void onStop() {
        chartController = null;
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @SuppressLint("SetTextI18n")
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceInfo deviceInfo) {
        if (deviceInfo != null) {
            this.deviceInfo = deviceInfo;
            ManagerStatsSensor.getInstance(getActivity()).Initialized(deviceInfo);
            StateDeviceModel.setText(deviceInfo.getVersion());
            StateDeviceMaxTimers.setText(valueOf(deviceInfo.getMax_timer()));
            StateDeviceMaxTempSensors.setText(valueOf(deviceInfo.getMax_temp_sensor()));
            StateDeviceMaxTemp.setText(format(Locale.ENGLISH, "%.0f", (float) deviceInfo.getMax_temp() / 100) + "C°");
            StateDeviceMinTemp.setText(format(Locale.ENGLISH, "%.0f", (float) deviceInfo.getMin_temp() / 100) + "C°");
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceCanalState deviceCanalState) {
        if (deviceCanalState != null) {
            if (_deviceCanalState == null || !_deviceCanalState.getCanal_timer().equals(deviceCanalState.getCanal_timer()) || !_deviceCanalState.getCanal().equals(deviceCanalState.getCanal())) {
                _deviceCanalState = deviceCanalState;
                UpDateStateChanal();
                UpdateChart();
            }
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceTempSensors deviceTempSensors) {
        if (deviceTempSensors != null && deviceTempSensors.getSensors() != null) {
            UpdateChartTemp();
            if (deviceTempSensors.getSensors().get(0) == 0) {
                StateDeviceTempSensor1.setText("--.--");
            } else {
                float temp1 = (float) (deviceTempSensors.getSensors().get(0) * 25 + this.deviceInfo.getMin_temp()) / 100;
                StateDeviceTempSensor1.setText(format(Locale.ENGLISH, "%.02f", temp1));
            }
            if (deviceTempSensors.getSensors().get(1) == 0) {
                StateDeviceTempSensor2.setText("--.--");
            } else {
                float temp2 = (float) (deviceTempSensors.getSensors().get(1) * 25 + this.deviceInfo.getMin_temp()) / 100;
                StateDeviceTempSensor2.setText(format(Locale.ENGLISH, "%.02f", temp2));
            }
        }

    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceTimer deviceTimer) {
        if (deviceTimer != null) {
            this.deviceTimer = deviceTimer;
            UpdateChart();
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceSecondsTimer deviceSecondsTimer) {
        if (deviceSecondsTimer != null) {
            this.deviceSecondsTimer = deviceSecondsTimer;
            UpdateChart();
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceHoursTimer deviceHoursTimer) {
        if (deviceHoursTimer != null)
            this.deviceHoursTimer = deviceHoursTimer;
        UpdateChart();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceTempState deviceTempState) {
        if (deviceTempState != null) {
            this.deviceTempState = deviceTempState;
            UpdateChart();
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(ManagerStatsSensor statsTempSensor) {
        if (statsTempSensor != null) {
            this.statsTempSensor = statsTempSensor;

            int temp = statsTempSensor.GetLastDataTemp(0);
            if (temp >= 0) {
                if (temp == 0) {
                    StateDeviceTempSensor1.setText("--.--");
                } else {
                    float temp1 = (float) (temp * 25 + this.deviceInfo.getMin_temp()) / 100;
                    StateDeviceTempSensor1.setText(format(Locale.ENGLISH, "%.02f", temp1));
                }
            }
            temp = statsTempSensor.GetLastDataTemp(1);
            if (temp >= 0) {
                if (temp == 0) {
                    StateDeviceTempSensor2.setText("--.--");
                } else {
                    float temp2 = (float) (temp * 25 + this.deviceInfo.getMin_temp()) / 100;
                    StateDeviceTempSensor2.setText(format(Locale.ENGLISH, "%.02f", temp2));
                }
            }

            UpdateChartTemp();
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        UpdateChart();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void UpDateStateChanal() {
        if (_deviceCanalState == null) return;
        GetStateCanalOnType(imageWater, R.drawable.ic_raindrop, R.drawable.ic_raindrop_off, StateDeviceFilters, "multi_select_list_preference_filter");
        GetStateCanalOnType(imageLight, R.drawable.ic_idea_main, R.drawable.ic_idea_off, StateDeviceLights, "multi_select_list_preference_light");
        GetStateCanalOnType(imageHeater, R.drawable.ic_water_heater, R.drawable.ic_water_heater_off, StateDeviceHeaters, "multi_select_list_preference_heater");
        GetStateCanalOnType(imageCO2, R.drawable.ic_co2, R.drawable.ic_co2_off, StateDeviceCO2, "multi_select_list_preference_co2");
        GetStateCanalOnType(imageFan, R.drawable.ic_propeller, R.drawable.ic_propeller_off, StateDeviceOther, "multi_select_list_preference_fan");

    }

    public void GetStateCanalOnType(ImageView image, int on, int off, TextView view, String key) {
        String message = "";
        switch (shareManager.GetStateCanalOnType(_deviceCanalState, key)) {

            case CANAL_ON:
                message = getString(R.string.ON);
                image.setImageResource(on);
                break;
            case CANAL_ON_OFF:
                message = getString(R.string.ON_OFF);
                image.setImageResource(on);
                break;
            case CANAL_OFF:
                message = getString(R.string.OFF);
                image.setImageResource(off);
                break;
            case CANAL_INDEFINED:
                message = "--";
                break;
        }


        switch (ShareManager.getInstance(getContext()).GetHandStateCanalOnType(_deviceCanalState, key)) {

            case CHANAL_OFF:
                message += "/" + getString(R.string.OFF);
                break;
            case CHANAL_ON:
                message += "/" + getString(R.string.ON);
                break;
            case CHANAL_AUTO:
                message += "/" + getString(R.string.AUTO);
                break;

        }
        view.setText(message);
    }

    private void UpdateChart() {
        if (_deviceCanalState != null && deviceInfo != null && deviceHoursTimer != null && deviceSecondsTimer != null && deviceTimer != null) {
            chartController = new ChartController(getActivity(), _deviceCanalState, deviceInfo, deviceTimer, deviceSecondsTimer, deviceHoursTimer);
            HelperLineChartView.Builder builder = new HelperLineChartView.Builder(new LineChart[]{chartPower}, HelperLineChartView.ChartView.CHART_VIEW_LAYOUT, getActivity());
            builder.SetChatLineDataSet(new List[]{chartController.GetEntriesPowerByHour()}, new int[]{R.drawable.fade_layout}, 0);
            builder.SetChatView();
            builder.SetXAxis();
            builder.SetYAxis();
            builder.Build();
            TextPowerHour.setText(getString(R.string.PowerValueHour) + format("%.2f", chartController.GetPowerStatistic().get(0)) + getString(R.string.PowerValueWatt));
            TextPowerDay.setText(getString(R.string.PowerValueDay) + format("%.2f", chartController.GetPowerStatistic().get(1)).toString() + getString(R.string.PowerValue));
            TextPowerMonth.setText(getString(R.string.PowerValueMonth) + format("%.2f", chartController.GetPowerStatistic().get(2)).toString() + getString(R.string.PowerValue));
        }
    }

    private void UpdateChartTemp() {
        if (statsTempSensor != null && deviceInfo != null) {
            chartController = new ChartController(getActivity(), deviceInfo, statsTempSensor);
            HelperLineChartView.Builder builder = new HelperLineChartView.Builder(new LineChart[]{chartTemp1, chartTemp2}, HelperLineChartView.ChartView.CHART_VIEW_BUTTON, getActivity());
            try {
                builder.SetChatLineDataSet(new List[]{chartController.GetEntriesForTemp(0), chartController.GetEntriesForTemp(1)}, new int[]{R.drawable.fade_list_green, R.drawable.fade_list_yellow}, 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            builder.SetChatView();
            builder.SetXAxis();
            builder.SetYAxis();
            builder.Build();
        }
    }


    private void OpenTempFragment() {
        try {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            Fragment prev = getFragmentManager().findFragmentByTag("dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);

            // Create and show the dialog.
            DialogFragment newFragment = ChartTempFragment.newInstance();
            newFragment.show(ft, "dialog");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ChangeCanalState(String key) {
        List<Integer> canals = ShareManager.getInstance(getContext()).GetCanalOnType(key);
        if (canals == null) {
            Snackbar.make(view, getString(R.string.CheckPreference), Snackbar.LENGTH_SHORT).setAction("Action", null).show();
            return;
        }
        int newState = CHANAL_AUTO;
        String message = "";
        switch (ShareManager.getInstance(getContext()).GetHandStateCanalOnType(_deviceCanalState, key)) {

            case CHANAL_OFF:
                newState = CHANAL_AUTO;
                message = getString(R.string.AUTO);
                break;
            case CHANAL_ON:
                message = getString(R.string.OFF);
                newState = CHANAL_OFF;
                break;
            case CHANAL_AUTO:
                message = getString(R.string.ON);
                newState = CHANAL_ON;
                break;

        }
        try {
            _changeDeviceCanalState = _deviceCanalState.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        ArrayList<Integer> currentCanal = _changeDeviceCanalState.getCanal_timer();
        for (Integer c : canals) {
            currentCanal.set(c, newState);
        }
        Snackbar.make(view, getString(R.string.SendDataCanal) + message, Snackbar.LENGTH_SHORT).setAction("Action", null).show();
        new RequestManager().SendPOSTRequest(ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_CANAL_STATE, _changeDeviceCanalState, StateFragment.this);
    }

    @Override
    public void onEvent(EventObject e) {

        if (e == null || _deviceCanalState.getCanal_timer() == null ||
                e.getSource() != ProtocolHelper.TaskResult.RESULT_SUCCESS ||
                !_deviceCanalState.getCanal_timer().equals(_changeDeviceCanalState.getCanal_timer())) {
            Log.e(getClass().getName(), "Invalid result Canal State");
            attemptCount--;
            if (attemptCount > 0)
                new RequestManager().SendPOSTRequest(ProtocolHelper.REQUEST_TYPE.REQUEST_DEVICE_CANAL_STATE, _changeDeviceCanalState, StateFragment.this);
            else {
                fab.setVisibility(View.VISIBLE);
                ToastHelper.ToastDeviceLog(e);
                if (getActivity() != null)
                    ((DeviceActivity) getActivity()).isWait = false;
            }
        } else {
            if (getActivity() != null)
                ((DeviceActivity) getActivity()).isWait = false;
            ToastHelper.ToastDeviceLog(e);
            _changeDeviceCanalState = null;

        }
    }
}
