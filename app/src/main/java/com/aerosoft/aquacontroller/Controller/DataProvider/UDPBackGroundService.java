package com.aerosoft.aquacontroller.Controller.DataProvider;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.aerosoft.aquacontroller.Controller.DataHelper.GsonHelper;
import com.aerosoft.aquacontroller.Controller.DataHelper.UdpHelper;
import com.aerosoft.aquacontroller.Controller.DataListener.IRequestListener;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceCanalState;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceTempSensors;
import com.aerosoft.aquacontroller.Model.DataModel.ManagerStatsSensor;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.View.Fragments.Preference.ShareManager;
import com.aerosoft.aquacontroller.View.SplashActivity;
import com.aerosoft.aquacontroller.View.ViewHelper.Helpers;
import com.crashlytics.android.Crashlytics;

import org.greenrobot.eventbus.EventBus;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.EventObject;
import java.util.Locale;
import java.util.Objects;

import io.fabric.sdk.android.Fabric;

import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_DEVICE_CANAL_SETTINGS;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_DEVICE_INFO;
import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.REQUEST_DEVICE_TEMP_SENSOR;

/**
 * Created by Vadim on 03.02.2017.
 */


public class UDPBackGroundService extends Service {

    public static final int DEFAULT_NOTIFICATION_ID = 101;

    private ManagerStatsSensor statsTempSensor;
    private IRequestListener mOnEventListener;
    private UdpServerThread udpServerThread;
    private NotificationManager notificationManager;
    private DeviceInfo deviceInfo;
    private DeviceTempSensors deviceTempSensor;
    private DeviceCanalState deviceCanalState;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {


    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        deviceTempSensor = null;
        deviceCanalState = null;
        deviceInfo = null;
        notificationManager = (NotificationManager) this.getSystemService(NOTIFICATION_SERVICE);

        ShareManager.getInstance(this).Initilized();
        DoTaskService();
        return START_REDELIVER_INTENT;
    }

    /**
     * Start Service work
     */
    private void DoTaskService() {
        statsTempSensor = ManagerStatsSensor.getInstance(getApplicationContext());
        try {
            sendNotification("");
        } catch (Exception e) {
            Log.d(this.getClass().getName(), e.getMessage());
            Crashlytics.log(e.getMessage());
        }
        if (udpServerThread == null) {
            Fabric.with(this, new Crashlytics());
            Log.d(this.getClass().getName(), "Create UDP Server!");
            udpServerThread = new UdpServerThread(new IRequestListener() {
                @Override
                public void onEvent(EventObject e) {

                    GsonHelper.ResultTaskResponse<Cloneable> result = GsonHelper.JSONtoObject(e.getSource().toString());
                    if (result != null) {
                        SendEvent(result);
                        try {
                            sendNotification("");
                        } catch (Exception e1) {
                            Log.d(this.getClass().getName(), e1.getMessage());
                            Crashlytics.log(e1.getMessage());
                        }
                    }
                }
            });
        }
        if (!udpServerThread.isAlive()) {
            Log.d(this.getClass().getName(), "Start UDP Server!");
            udpServerThread.Start();
        }
    }

    /**
     * Send event to Application
     *
     * @param result
     */
    private void SendEvent(final GsonHelper.ResultTaskResponse<Cloneable> result) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {

                if (result.GetTypeMessage() == REQUEST_DEVICE_TEMP_SENSOR) {
                    //get data from temperature sensors
                    deviceTempSensor = (DeviceTempSensors) result.GetResult().getResponse();
                    try {
                        if (statsTempSensor.SetDataTemp(result.GetResult())) {
                            EventBus.getDefault().postSticky(statsTempSensor);
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                } else if (result.GetTypeMessage() == REQUEST_DEVICE_INFO) {
                    //get data from device info
                    if (deviceInfo == null) {
                        deviceInfo = (DeviceInfo) result.GetResult().getResponse();
                        ManagerStatsSensor.getInstance(UDPBackGroundService.this).Initialized(deviceInfo);
                    }
                } else if (result.GetTypeMessage() == REQUEST_DEVICE_CANAL_SETTINGS) {
                    //get data from device canal state sensors
                    deviceCanalState = (DeviceCanalState) result.GetResult().getResponse();
                    EventBus.getDefault().postSticky(deviceCanalState);
                }


            }
        });
    }

    /**
     * UDP Server listener
     */
    private class UdpServerThread {
        private MyDatagramReceiver myDatagramReceiver;
        private Runnable ActionEventListener = new Runnable() {
            public void run() {
                if (myDatagramReceiver == null) return;
                if (mOnEventListener != null)
                    mOnEventListener.onEvent(new EventObject(myDatagramReceiver.getLastMessage()));
            }
        };

        UdpServerThread(IRequestListener listener) {
            mOnEventListener = listener;
        }

        void Start() {
            myDatagramReceiver = new MyDatagramReceiver();
            myDatagramReceiver.start();
        }

        boolean isAlive() {
            return myDatagramReceiver != null && myDatagramReceiver.isAlive();
        }

        public void onResume() {
            Start();
        }

        public void onPause() {
            myDatagramReceiver.kill();
        }

        private class MyDatagramReceiver extends Thread {
            final Handler myHandler = new Handler();
            private boolean bKeepRunning = true;
            private String lastMessage = "";

            public void run() {
                String message;
                byte[] lmessage = new byte[UdpHelper.MAX_UDP_DATAGRAM_LEN];
                DatagramPacket packet = new DatagramPacket(lmessage, lmessage.length);

                try {
                    DatagramSocket socket = new DatagramSocket(UdpHelper.PORT);

                    while (bKeepRunning) {
                        socket.receive(packet);
                        if (packet != null && packet.getAddress().getHostAddress().equals(UdpHelper.ADDRESS)) {
                            message = new String(lmessage, 0, packet.getLength());
                            lastMessage = message;
                            myHandler.post(ActionEventListener);
                        }
                    }
                    socket.close();
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }

            private void kill() {
                bKeepRunning = false;
            }

            @org.jetbrains.annotations.Contract(pure = true)
            private String getLastMessage() {
                return lastMessage;
            }
        }
    }

    /**
     * Send custom notification
     *
     * @param Ticker
     * @throws Exception
     */

    public void sendNotification(String Ticker) throws Exception {

        //These three lines makes Notification to open main activity after clicking on it
        Intent notificationIntent = new Intent(this, SplashActivity.class);
        notificationIntent.setAction(Intent.ACTION_MAIN);
        notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        String channelId = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channelId = createNotificationChannel();
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId);
        builder.setContentIntent(contentIntent).setOngoing(true).setSmallIcon(R.drawable.ic_fish_action_bar);
        if (statsTempSensor != null && deviceInfo != null && deviceTempSensor != null && deviceTempSensor.getSensors() != null) {
            builder.setLargeIcon(Helpers.textAsBitmap(String.format(Locale.ENGLISH, "%.0f", (float) (statsTempSensor.GetLastDataTemp(0) * 25 + deviceInfo.getMin_temp()) / 100) + "°", 80, Color.BLACK));
        } else {
            builder.setLargeIcon(Helpers.textAsBitmap("--°", 80, Color.BLACK));
        }
        builder.setTicker(Ticker);
        builder.setContentTitle(getString(R.string.app_name)); //Заголовок
        if (deviceInfo != null && deviceCanalState != null && deviceCanalState.getCanal().size() == deviceInfo.GetMaxCanal()) {
            String StateCanalText = "";
            for (int i = 1; i < deviceCanalState.getCanal().size() + 1; i++) {
                if (deviceCanalState.getCanal().get(i - 1) == 1) StateCanalText += "_";
                else
                    StateCanalText += i;
            }
            builder.setContentText("Canal: <" + StateCanalText + "> " + UdpHelper.ADDRESS);
        } else {
            builder.setContentText("Wait for update " + UdpHelper.ADDRESS);
        }
        builder.setWhen(System.currentTimeMillis());
        Notification notification;
        notification = builder.build();
        startForeground(DEFAULT_NOTIFICATION_ID, notification);
    }
    private String createNotificationChannel(){
        String channelId = "AquaController";
        String channelName = "Background Service";
        NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_NONE);
        channel.setLightColor(Color.BLUE);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(channel);
        return channelId;
    }

    @Override
    public void onDestroy() {
        udpServerThread = null;
        super.onDestroy();

    }
}
