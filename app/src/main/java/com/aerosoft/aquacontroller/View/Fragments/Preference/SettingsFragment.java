package com.aerosoft.aquacontroller.View.Fragments.Preference;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.preference.Preference;

import com.aerosoft.aquacontroller.Controller.DataHelper.Constants;
import com.aerosoft.aquacontroller.Controller.DataHelper.UdpHelper;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Model.DataModel.ManagerStatsSensor;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.View.SplashActivity;
import com.takisoft.fix.support.v7.preference.PreferenceFragmentCompat;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Arrays;
import java.util.List;


public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

    List<String> listPreferName = Arrays.asList("pref_power_1", "pref_power_2", "pref_power_3", "pref_power_4", "pref_power_5", "pref_power_6", "pref_power_7", "pref_power_8");

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPreferenceManager().setSharedPreferencesName(UdpHelper.ADDRESS);
        addPreferencesFromResource(R.xml.preference);

    }

    @Override
    public void onCreatePreferencesFix(@Nullable Bundle savedInstanceState, String rootKey) {

    }

    @Override
    public void onStart() {
        super.onStart();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
        EventBus.getDefault().unregister(this);

    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceInfo deviceInfo) {
        if (deviceInfo != null) {
            for (int i = 0; i < listPreferName.size(); i++) {
                if (deviceInfo.GetMaxCanal() - 1 < i) {
                    Preference preference = findPreference(listPreferName.get(i));
                    preference.setVisible(false);
                }
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        if (key.equals("gmt_zone")) {
            SharedPreferences preferenceIp = getContext().getSharedPreferences(Constants.IP_PREF, Context.MODE_PRIVATE);
            SharedPreferences.Editor prefEditor = preferenceIp.edit();
            String TimeZone = sharedPreferences.getString("gmt_zone", "");
            prefEditor.putString("gmt_zone", TimeZone);
            prefEditor.commit();
        }

        if (key.equals("ip_address")) {
            //input current IP
            String IP = sharedPreferences.getString("ip_address", "");
            String TimeZone = sharedPreferences.getString("gmt_zone", "");

            if (IP.length() > 0 && Constants.PATTERN.matcher(IP).matches()) {
                ShareManager.getInstance(getContext()).AddIPDevices(IP);
                //set IP in global preference
                SharedPreferences preferenceIp = getContext().getSharedPreferences(Constants.IP_PREF, Context.MODE_PRIVATE);
                SharedPreferences.Editor prefEditor = preferenceIp.edit();
                prefEditor.putString("ip_address", IP);
                prefEditor.putString("gmt_zone", TimeZone);
                prefEditor.commit();

                SharedPreferences preferenceNext = getContext().getSharedPreferences(IP, Context.MODE_PRIVATE);
                prefEditor = preferenceNext.edit();
                prefEditor.putString("ip_address", IP);
                prefEditor.putString("gmt_zone", TimeZone);
                prefEditor.commit();

                UdpHelper.ADDRESS = IP;
                //remote IP from current pref
                prefEditor = sharedPreferences.edit();
                prefEditor.remove("gmt_zone");
                prefEditor.remove("ip_address");
                prefEditor.commit();

                Intent splashActivity = new Intent(getContext(),
                        SplashActivity.class);
                ManagerStatsSensor.getInstance(getContext()).OnChangeDevice();
                startActivity(splashActivity);
            }


        }

    }
}
