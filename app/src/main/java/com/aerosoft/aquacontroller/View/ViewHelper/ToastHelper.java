package com.aerosoft.aquacontroller.View.ViewHelper;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.*;

import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.aerosoft.aquacontroller.R;

import java.util.EventObject;

/**
 * Created by Vadim on 18.11.2016.
 */

public class ToastHelper {

    private static Context _context;
    private static boolean isInit = false;

    public static void Initilized(Context context) {

        _context = context;
        isInit = true;
    }

    public static void ToastDeviceLog(Context context, TaskResult result) {
        if (context != null) {
            Toast.makeText(context, GetMessage(result), Toast.LENGTH_LONG).show();
        }
    }

    public static void ToastDeviceLog(EventObject object) {
        if (_context != null && object != null && isInit) {
            Toast.makeText(_context, GetMessage((ProtocolHelper.TaskResult) object.getSource()), Toast.LENGTH_SHORT).show();
        }
    }
    private static String GetMessage(TaskResult result) {
        switch (result) {

            case RESULT_INIT_LOGO:
                return "RESULT_INIT_LOGO";

            case RESULT_SETTINGS:
                return "RESULT_SETTINGS";

            case RESULT_CONNECTION:
                return "RESULT_CONNECTION";

            case RESULT_DEVICE_INFO:
                return "RESULT_DEVICE_INFO";

            case RESULT_DEVICE_CHANAL_STATE:
                return "RESULT_DEVICE_CHANAL_STATE";

            case RESULT_DEVICE_TIME_DAILY:
                return "RESULT_DEVICE_TIME_DAILY";

            case RESULT_DEVICE_TIME_HOUR:
                return "RESULT_DEVICE_TIME_HOUR";

            case RESULT_DEVICE_TIME_SECOND:
                return "RESULT_DEVICE_TIME_DAILY";

            case RESULT_DEVICE_TEMP:
                return "RESULT_DEVICE_TEMP";

            case RESULT_SUCCESS:
                return "RESULT_SUCCESS";

            case RESULT_ERROR_SOCKET_EXCEPTION:
                return "ERROR_SOCKET_EXCEPTION : " + _context.getString(R.string.SOCKET_EXCEPTION);
            case RESULT_ERROR_UNKNOWN_HOST_EXCEPTION:
                return "ERROR_UNKNOWN_HOST_EXCEPTION : " + _context.getString(R.string.UNKNOWN_HOST_EXCEPTION);
            case RESULT_ERROR_IO_EXCEPTION:
                return "ERROR_IO_EXCEPTION : " + _context.getString(R.string.SOCKET_EXCEPTION);
            case RESULT_ERROR_SOCKET_TIMEOUT_EXCEPTION:
                return "ERROR_SOCKET_TIMEOUT_EXCEPTION : " + _context.getString(R.string.SOCKET_TIMEOUT_EXCEPTION);
            case RESULT_ERROR_NOT_RESPONSE:
                return "ERROR_NOT_RESPONSE : " + _context.getString(R.string.NOT_RESPONSE);
            case RESULT_DATA_CORRUPTED:
                return "DATA_CORRUPTED : " + _context.getString(R.string.DATA_CORRUPTED);
            case RESULT_DATA_DEVICE_BUSY:
                return  _context.getString(R.string.RESULT_DATA_DEVICE_BUSY);
        }
        return "";
    }

    public static Snackbar SnackBarShow(View view, String message){
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE)
                .setAction("Action", null);
        snackbar.show();
        return snackbar;
    }
    public static Snackbar SnackBarShow(View view, TaskResult result){
        Snackbar snackbar = Snackbar.make(view, GetMessage(result), Snackbar.LENGTH_INDEFINITE)
                .setAction("Action", null);
        snackbar.show();
        return snackbar;
    }
}
