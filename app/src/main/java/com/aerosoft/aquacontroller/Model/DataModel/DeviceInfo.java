package com.aerosoft.aquacontroller.Model.DataModel;

import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Vadim on 11.11.2016.
 */

public class DeviceInfo implements Cloneable, IValidateData {

    @SerializedName("ver")
    private String version;

    @SerializedName("m_t")
    private int max_timer;

    @SerializedName("m_t_se")
    private int max_temp_sensor;

    @SerializedName("min_t")
    private int min_temp;

    @SerializedName("max_t")
    private int max_temp;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getMax_timer() {
        return max_timer;
    }

    public int getMax_temp_sensor() {
        return max_temp_sensor;
    }

    public int getMin_temp() {
        return min_temp;
    }

    public int getMax_temp() {
        return max_temp;
    }


    public DeviceInfo clone() throws CloneNotSupportedException {
        DeviceInfo newDeviceTempState = (DeviceInfo) super.clone();
        return newDeviceTempState;
    }

    public int GetMaxCanal() {
        if (version.equals(ProtocolHelper.DEVICE_TYPE.get(0)))
            return 4;
        else if (version.equals(ProtocolHelper.DEVICE_TYPE.get(1)))
            return 8;
        else if (version.equals(ProtocolHelper.DEVICE_TYPE.get(2)))
            return 8;
        else return -1;
    }

    @Override
    public boolean IsValidate() {
        if (version.length() == 0 || max_timer == 0 || max_temp_sensor == 0 || min_temp == 0 || max_temp == 0)
            return false;
        return GetMaxCanal() != -1;

    }
}
