package com.aerosoft.aquacontroller.Controller.DataListener;

import com.aerosoft.aquacontroller.Model.DataModel.DeviceTempState;

/**
 * Created by Vadim on 06.12.2016.
 * Interface for listener changed DataTempState in List Adapter
 */

public interface IDataTempStateListener {
    void onEvent(DeviceTempState e);
}
