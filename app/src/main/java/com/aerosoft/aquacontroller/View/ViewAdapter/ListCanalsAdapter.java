package com.aerosoft.aquacontroller.View.ViewAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.aerosoft.aquacontroller.Model.DataModel.DeviceCanalState;
import com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper;
import com.aerosoft.aquacontroller.Model.DataModel.DeviceInfo;
import com.aerosoft.aquacontroller.Controller.DataListener.IDataCanalEventListener;
import com.aerosoft.aquacontroller.R;
import com.aerosoft.aquacontroller.View.Fragments.Preference.ShareManager;

import java.util.ArrayList;

import static com.aerosoft.aquacontroller.Controller.DataHelper.ProtocolHelper.*;
import static com.aerosoft.aquacontroller.R.string.DataErrorText;

/**
 * Created by Doc on 20.11.2016.
 */

public class ListCanalsAdapter extends BaseAdapter {

    private final Context context;
    private final DeviceCanalState deviceCanalState;
    private final int[] imagesID = {R.drawable.ic_numeric_1_box, R.drawable.ic_numeric_2_box, R.drawable.ic_numeric_3_box, R.drawable.ic_numeric_4_box, R.drawable.ic_numeric_5_box, R.drawable.ic_numeric_6_box, R.drawable.ic_numeric_7_box, R.drawable.ic_numeric_8_box};
    ArrayList<Integer> objects;
    private DeviceInfo deviceInfo;
    private IDataCanalEventListener listner;

    public ListCanalsAdapter(Context context, DeviceCanalState deviceCanalState, DeviceInfo deviceInfo, IDataCanalEventListener listener) {
        this.context = context;
        this.deviceCanalState = deviceCanalState;
        this.deviceInfo = deviceInfo;
        this.listner = listener;
        objects = deviceCanalState.getCanal();
    }

    @Override
    public int getCount() {
        return deviceInfo.GetMaxCanal();

    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.chanal_list_item, parent, false);
        TextView textView = rowView.findViewById(R.id.chanalDeviceStateText);
        TextView canalName = rowView.findViewById(R.id.canalName);
        ImageView imageView = rowView.findViewById(R.id.chanalDeviceImage);
        SeekBar seekBar = rowView.findViewById(R.id.chanalDeviceSeekBar);
        imageView.setImageResource(imagesID[position]);
        seekBar.setProgress(deviceCanalState.getCanal_timer().get(position) - 1);
        canalName.setText(ShareManager.getInstance(context).GetPowerCanalName(position));
        switch (deviceCanalState.getCanal().get(position)) {
            case CHANAL_TIMER_OFF:
                textView.setText(context.getString(R.string.OFF));
                break;
            case CHANAL_TIMER_ON:
                textView.setText(context.getString(R.string.ON));
                break;
            case CHANAL_TIMER_MIN:
                textView.setText(context.getString(R.string.MIN));
                break;
            case CHANAL_TIMER_OTHER:
                textView.setText(context.getString(R.string.OTHER));
                break;
            case CHANAL_TIMER_SEC:
                textView.setText(context.getString(R.string.SEC));
                break;
            case CHANAL_TIMER_TEMP:
                textView.setText(context.getString(R.string.TEMP));
                break;
            case CHANAL_TIMER_PWM:
                textView.setText(context.getString(R.string.PWM));
                break;
            case CHANAL_TIMER_PH:
                textView.setText(context.getString(R.string.PH));
                break;
            default:
                textView.setText(context.getString(DataErrorText));
                break;

        }
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                deviceCanalState.getCanal_timer().set(position, progress + 1);
                Log.d(getClass().getName().toString(), deviceCanalState.getCanal_timer().toString());
                listner.onEvent(deviceCanalState);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        return rowView;
    }
}
