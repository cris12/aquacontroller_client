package com.aerosoft.aquacontroller.Controller.DataHelper;

import com.aerosoft.aquacontroller.BuildConfig;

import java.net.DatagramPacket;

/**
 * Created by Vadim on 10.11.2016.
 */

public class UdpHelper {


    public static final int MAX_UDP_DATAGRAM_LEN = 350;
    public static final int MAX_UDP_TIMEOUT = 5000;
    public static int PORT = 8888;

    public static boolean IS_TEST = BuildConfig.DEBUG;
    public static String ADDRESS = "";
    public static String REQUEST_NULL = "Request complete";
    public static String RESPONSE_ERROR = "error";
    public static String RESPONSE_SUCCESS = "success";
    public static String REQUEST_DEVICE_FIELD_NAME = "message";


    /**
     * Converts a given datagram packet's contents to a String.
     */
    public static String stringFromPacket(DatagramPacket packet) {
        return new String(packet.getData(), 0, packet.getLength());
    }

    /**
     * Converts a given String into a datagram packet.
     */
    public static DatagramPacket stringToPacket(String s, DatagramPacket packet) {
        byte[] bytes = s.getBytes();
        System.arraycopy(bytes, 0, packet.getData(), 0, bytes.length);
        packet.setLength(bytes.length);
        return packet;
    }

}
