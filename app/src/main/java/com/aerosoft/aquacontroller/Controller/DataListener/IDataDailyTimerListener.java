package com.aerosoft.aquacontroller.Controller.DataListener;

import com.aerosoft.aquacontroller.Model.DataModel.DeviceTimer;

/**
 * Created by Vadim on 06.12.2016.
 * Interface for listener changed DataDailyTimer in List Adapter
 */

public interface IDataDailyTimerListener {
    void onEvent(DeviceTimer e);
}
